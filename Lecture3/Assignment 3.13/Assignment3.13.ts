const month = Number(process.argv[2]);

const monthNames: string[] = [
    "Tammikuu",
    "Helmikuu",
    "Maaliskuu",
    "Huhtikuu",
    "Toukokuu",
    "Kesäkuu",
    "Heinäkuu",
    "Elokuu",
    "Syyskuu",
    "Lokakuu",
    "Marraskuu",
    "Joulukuu"
];

const daysInMonth: number[] = [
    31, 28, 31, 30, 31, 30,
    31, 31, 30, 31, 30, 31
];

if (month >= 1 && month <= 12) {
    console.log(`Päiviä on ${daysInMonth[month - 1]} ${monthNames[month - 1]}`);
} else {
    console.log("Vuodessa on 12 kuukautta, valitse niistä yksi!");
}
