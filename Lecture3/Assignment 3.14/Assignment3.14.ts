
const balance: number = 14.0;
const checkBalance  = true;
const isActive  = true;

if (checkBalance && isActive && balance > 0.0) {
    console.log("Your balance is " + balance + "€");
} 
else if (!checkBalance) {
    console.log("Have a nice day!");
} 
else if (checkBalance && !isActive) {
    console.log("Your account is no longer active.");
} 
else if (checkBalance && isActive && balance === 0.0) {
    console.log("Your account is empty.");
} 
else if (checkBalance && isActive && balance < 0.0) {
    console.log("Your account is negative.");
}
