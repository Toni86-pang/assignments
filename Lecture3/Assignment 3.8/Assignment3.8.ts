
const turn = 17;
let mult = 1;
let sum1 = 0;
while (mult <= turn) {
    if (mult % 3 === 0 || mult % 5 === 0) {
        sum1 += mult;
    }
    mult = mult + 1;
}
console.log(sum1);
