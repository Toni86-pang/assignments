"use strict";
const oldCharacter = (process.argv[2]);
const newCharacter = (process.argv[3]);
const str = (process.argv[4]);
// typescript on sitä mieltä että replaceAll on huono malli mutta tämä toteuttaa tarvittavan toimen replacen sijasta.
console.log(str.replaceAll(oldCharacter, newCharacter));
