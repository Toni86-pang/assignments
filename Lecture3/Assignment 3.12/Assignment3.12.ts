const a = Number(process.argv[2]);
const b = Number(process.argv[3]);
const c = Number(process.argv[4]);

if (typeof a !== "number" || typeof b !== "number" || typeof c !== "number") {
    console.log("Please provide three valid numbers as command-line arguments.");
} else {
    console.log(Math.max(a, b, c), "is the largest number");
    console.log(Math.min(a, b, c), "is the smallest number");

    if (a === b && b === c) {
        console.log(a, b, c, "all are equal");
    }

    console.log(a, "is a,", b, "is b,", c, "is c");
}