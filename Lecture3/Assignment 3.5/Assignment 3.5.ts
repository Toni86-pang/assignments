
const myArray = ["banaani", "omena", "mandariini", "appelsiini", "kurkku", "tomaatti", "peruna"];
//1
console.log("1.");
console.log(myArray[2], myArray[4]);
//2
console.log("2.");
console.log(myArray.sort());
//3
console.log("3.");
myArray.push("sipuli");
console.log(myArray);
//4
console.log("4.");
myArray.shift();
console.log(myArray);
//5
console.log("5.");
myArray.forEach(element => console.log(element));
//6
console.log("6");
