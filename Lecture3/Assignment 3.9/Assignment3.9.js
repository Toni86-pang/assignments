"use strict";
const turn1 = 100;
let divd = 1;
while (divd <= turn1) {
    if (divd % 3 === 0 && divd % 5 === 0) {
        console.log("fizzbuzz");
    }
    else if (divd % 3 === 0) {
        console.log("Fizz");
    }
    else if (divd % 5 === 0) {
        console.log("buzz");
    }
    else {
        console.log(divd);
    }
    divd = divd + 1;
}
