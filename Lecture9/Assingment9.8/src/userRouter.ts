import express, { Request, Response, NextFunction } from 'express';
import argon2 from 'argon2';
import jwt from 'jsonwebtoken';
import dotenv from 'dotenv';

dotenv.config();

interface User {
  username: string;
  password: string;
}

interface CustomRequest extends Request {
    user?: User;
  }

const users: User[] = [];

const router = express.Router();



router.post('/register', (req: CustomRequest, res: Response) => {
    const { username, password } = req.body;

    argon2
        .hash(password)
        .then((hash) => {
            users.push({ username, password: hash });
            console.log(users);
            res.status(201).send(); // register ei lähetä tokenia
        })
        .catch((error) => {
            console.error('Error during password hashing:', error);
            res.status(500).send();
        });
});

router.post('/login', (req: CustomRequest, res: Response) => {
    const { username, password } = req.body;
    const user = users.find((user) => user.username === username);

    if (!user) {
        return res.status(401).send();
    }

    argon2
        .verify(user.password, password)
        .then((passwordMatches) => {
            if (passwordMatches) {
                const token = jwt.sign({ username: user.username }, process.env.SECRET || '', {
                    expiresIn: '15m',
                });
                return res.status(200).json({ token }); // .send lähettää myös json-objekteja, voi käyttää samaa metodia joka kerta
            } else {
                return res.status(401).send();
            }
        })
        .catch((error) => {
            console.error('Error during password verification:', error);
            return res.status(500).send();
        });
});

const authenticateJWT = (req: CustomRequest, res: Response, next: NextFunction) => {
    const authHeader = req.headers.authorization;
  
    if (authHeader) {
        const token = authHeader.split(' ')[1];
  
        jwt.verify(token, process.env.SECRET || '', (err, user) => {
            if (err) {
                return res.sendStatus(403);
            }
  
            req.user = user as User;
            next();
        });
    } else {
        res.sendStatus(401);
    }
};


router.use(authenticateJWT);


router.get('/protected-route', (req: CustomRequest, res: Response) => {
    res.send('This is a protected route');
});

export default router;
