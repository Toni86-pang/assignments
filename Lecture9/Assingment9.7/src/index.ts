import express from 'express';
import 'dotenv/config'
import jwt from 'jsonwebtoken'

const server = express();


server.use(express.static('public'));


server.use(express.json());


server.use('/', );


const secret = process.env.SECRET
if (secret === undefined) throw new Error('Missing SECRET environment variable')

const options = { expiresIn: '30m' }

const generateToken = (payload: { data: string }) => jwt.sign(payload, secret, options)

const token = generateToken({ data: process.argv[2] })
console.log(token)

try {
    const decodedToken = jwt.verify(token, secret)
    console.log(decodedToken)
} catch (error) {
    console.log('Invalid token')
}

const port = 3000;
server.listen(port, () => {
    console.log('Server listening on port', port);
});
