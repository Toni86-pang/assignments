import express, { Request, Response } from 'express';
import argon2 from 'argon2';

interface User {
  username: string;
  password: string;
}

const users: User[] = [];

const router = express.Router();

router.get('/', (_req: Request, res: Response) => {
    res.send(users.map(user => user.username));
});

router.post('/register', (req: Request, res: Response) => {
    const { username, password } = req.body;

    argon2
        .hash(password)
        .then(hash => {
            users.push({ username, password: hash });
            console.log(users);
            res.status(201).send();
        })
        .catch(error => {
            console.error('Error during password hashing:', error);
            res.status(500).send();
        });
});

router.post('/login', (req: Request, res: Response) => {
    const { username, password } = req.body;
    const user = users.find(user => user.username === username);

    if (!user) {
        return res.status(401).send();
    }

    argon2
        .verify(user.password, password)
        .then(passwordMatches => {
            if (passwordMatches) {
                return res.status(204).send();
            } else {
                return res.status(401).send();
            }
        })
        .catch(error => {
            console.error('Error during password verification:', error);
            return res.status(500).send();
        });
});

export default router;
