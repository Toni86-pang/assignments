import express from 'express';
import helmet from 'helmet';

import { logger, errorHandler , authenticate } from './middleware';

import booksRouter from './booksRouter';
import usersRouter from './usersRouter';

const server = express();

server.use(express.json());
server.use(helmet());
server.use(logger);
server.use(errorHandler);

server.use('/api/v1/users' ,authenticate, usersRouter);
server.use('/api/v1/books', booksRouter);

server.listen(3000, () => {
  console.log('Server running on port 3000');
});

export default server

// Siellä oli muutamat tehtävät vähän väärissä kansioissa, joten kommentit saattaa olla sekavia. Näytät preferoivan then/catch syntaksia asynkronisissa funktioissa. Harjoituksen vuoksi suosittelen ottamaan käyttöön myös async/await syntaksin.