import { Request, Response, NextFunction } from 'express';
import jwt from 'jsonwebtoken'
import dotenv from 'dotenv';

dotenv.config();

interface AdminRequest extends Request {
	username?: string
	isAdmin?: boolean
}

export const logger = (req: Request, _res: Response, next: NextFunction) => {
	const time = new Date();
	const logger = {
		url: req.url,
		time: time,
		method: req.method,
		body: null
	};
	if (req.body) {
		logger.body = req.body;
	}

	console.log(`[${logger.method}] ${logger.url}`);
	console.log('Body:', logger.body);
	next();
};

export const validateBookData = (req: Request, res: Response, next: NextFunction) => {
	const { id, name, author, read } = req.body;

	if (!id || !name || !author || read === undefined) {
		res.status(400).send('Invalid book data');
	} else {
		next();
	}
};

export const authenticate = (req: AdminRequest, res: Response, next: NextFunction ) => {
	const auth = req.get('Authorization')
	
	if (!auth?.startsWith('Bearer ')){
		return res.status(401).send('Invalid token 1')
	}

	const token = auth.substring(7)
	const secret = process.env.SECRET ?? ''
	try{
		const decodedToken = jwt.verify(token, secret)
		if(typeof decodedToken === 'string'){
			return res.status(401).send('invalid token')
		}
		req.username = decodedToken.username
		req.isAdmin = decodedToken.isAdmin
		next()
	} catch(error){
		return res.status(401).send('Invalid token 2')
	}
}

export const isAdmin =  (req: AdminRequest, res: Response, next: NextFunction) => {
	if (req.isAdmin){
		next()
	} else {
		res.status(401).send ('Missing admin priviledges')
	}
}

export const errorHandler = (err: Error, req: Request, res: Response, next: NextFunction) => {
  console.error(err);
  res.status(500).json({ error: 'Unknown error occurred.' });
  next(err); 
};

