import express, { Request, Response } from 'express';
import argon2 from 'argon2';
import jwt from 'jsonwebtoken';
import dotenv from 'dotenv';

dotenv.config();

interface User {
  username: string;
  password: string;
  isAdmin?: boolean;
}

let users: Array<User> = [];
const secret = process.env.SECRET;

export const usersRouter = express.Router();

usersRouter.post('/register', async (req: Request, res: Response) => {
  const { username, password } = req.body;

  const existingUser = users.find((user) => user.username === username);

  if (existingUser !== undefined) {
    return res.status(400).send('Registration failed: Username already exists');
  }

  if (username === process.env.ADMIN_USER) {
    return res.status(400).send('Username is invalid');
  }

  const hash = await argon2.hash(password);
  const newUser: User = {
    username,
    password: hash, // Store the hashed password
  };

  users.push(newUser);
  console.log('User registered:', newUser);

  const token = jwt.sign({ username }, process.env.SECRET || '', { expiresIn: '1h' });
  res.status(200).send(token); // Return a status code of 200 for success
});

usersRouter.post('/login', async (req: Request, res: Response) => {
  const { username, password } = req.body;
  const user = users.find((user) => user.username === username);

  if (username === process.env.ADMIN_USER) {
    const adminHash = process.env.ADMIN_HASH ?? '';
    const isValidAdminPassword = await argon2.verify(adminHash, password);

    if (!isValidAdminPassword) {
      return res.status(401).send('Incorrect username or password');
    }

    const token = jwt.sign({ username, isAdmin: true }, process.env.SECRET || '', { expiresIn: '1h' });
    return res.send({ token });
  }

  if (user === undefined) {
    return res.status(401).send('Incorrect username or password');
  }

  const passwordMatches = await argon2.verify(user.password, password); // Verify the hashed password

  if (passwordMatches) {
    const token = jwt.sign({ username }, process.env.SECRET || '', { expiresIn: '15m' });
    return res.status(200).json({ token });
  } else {
    return res.status(401).send('Incorrect username or password');
  }
});

export default usersRouter;
