import request from 'supertest';
import server from '../src/index';

beforeEach(async () => {
  await request(server).post('/api/v1/users/register').send({
    username: 'testuser',
    password: 'testpassword',
  });
});

describe('/api/v1/users/register', () => {
  it('returns a token on success', async () => {
    const response = await request(server).post('/api/v1/users/register').send({
      username: 'testuser2',
      password: 'testpassword',
    });
    expect(response.statusCode).toBe(200);
    expect(response.body.token).toBeDefined();
    // no niin, täällähän se on tarkistettu
  });

  it('does not allow to register admin', async () => {
    const response = await request(server).post('/api/v1/users/register').send({
      username: 'admin',
      password: 'testpassword',
    });
    expect(response.statusCode).toBe(400);
  });

  it('does not allow duplicate usernames', async () => {
    const response = await request(server).post('/api/v1/users/register').send({
      username: 'testuser',
      password: 'testpassword',
    });
    expect(response.statusCode).toBe(400);
  });
});

describe('/api/v1/users/login', () => {
  it('returns token on valid credentials', async () => {
    const response = await request(server).post('/api/v1/users/login').send({
      username: 'testuser',
      password: 'testpassword',
    });
    expect(response.statusCode).toBe(200);
    expect(response.body.token).toBeDefined();
  });

  it('returns 401 on invalid username', async () => {
    const response = await request(server).post('/api/v1/users/login').send({
      username: 'invaliduser',
      password: 'testpassword',
    });
    expect(response.statusCode).toBe(401);
  });

  it('returns 401 on invalid password', async () => {
    const response = await request(server).post('/api/v1/users/login').send({
      username: 'testuser',
      password: 'invalidpassword',
    });
    expect(response.statusCode).toBe(401);
  });
});
