import { Request, Response, NextFunction } from 'express';

export const logger = (req: Request, _res: Response, next: NextFunction) => {
	const time = new Date();
	const logger = {
		url: req.url,
		time: time,
		method: req.method,
		body: null
	};

	if (req.body) {
		logger.body = req.body;
	}

	console.log(`[${logger.method}] ${logger.url}`);
	console.log('Body:', logger.body);
	next();
};

export const validateBookData = (req: Request, res: Response, next: NextFunction) => {
	const { id, name, author, read } = req.body;

	if (!id || !name || !author || read === undefined) {
		res.status(400).send('Invalid book data');
	} else {
		next();
	}
};

export const errorHandler = (err: Error, req: Request, res: Response, next: NextFunction) => {
  console.error(err);
  res.status(500).json({ error: 'Unknown error occurred.' });
  next(err); 
};
