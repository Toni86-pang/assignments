import express, { Request, Response, NextFunction } from 'express';
import { logger, validateBookData, errorHandler } from './middleware';

const booksRouter = express.Router();

interface Book {
  id: number;
  name: string;
  author: string;
  read: boolean;
}

const books: Book[] = [];

booksRouter.use(logger);

booksRouter.get('/', (_req, res) => { // tyypit puuttuvat kaikista 
  res.json(books);
});

booksRouter.get('/:id', (req, res) => {
  const id = Number(req.params.id);
  const book = books.find((book) => book.id === id);
  if (book) {
    res.json(book);
  } else {
    res.status(404).send('Book not found');
  }
});

booksRouter.post('/', validateBookData, (req, res) => {
  const { id, name, author, read } = req.body;
  const newBook: Book = {
    id: Number(id),
    name: String(name),
    author: String(author),
    read: Boolean(read),
  };
  books.push(newBook);
  res.status(201).send('Book created');
});

booksRouter.put('/:id', validateBookData, (req, res) => {
  const id = Number(req.params.id);
  const bookIndex = books.findIndex((book) => book.id === id);
  if (bookIndex === -1) {
    res.status(404).send('Book not found');
  } else {
    const { name, author, read } = req.body;
    const updatedBook: Book = {
      id: id,
      name: String(name),
      author: String(author),
      read: Boolean(read),
    };
    books[bookIndex] = updatedBook;
    res.status(204).send();
  }
});

booksRouter.delete('/:id', (req, res) => {
  const id = Number(req.params.id);
  const bookIndex = books.findIndex((book) => book.id === id);
  if (bookIndex === -1) {
    res.status(404).send('Book not found');
  } else {
    books.splice(bookIndex, 1);
    res.status(204).send();
  }
});

booksRouter.use(errorHandler);

export default booksRouter;
