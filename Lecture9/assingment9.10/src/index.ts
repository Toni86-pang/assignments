import express from 'express';
import helmet from 'helmet';

import { logger, errorHandler } from './middleware';

import booksRouter from './booksRouter';
import usersRouter from './usersRouter';

const server = express();

server.use(express.json());
server.use(helmet());
server.use(logger);
server.use(errorHandler);

server.use('/api/v1/users', usersRouter);
server.use('/api/v1/books', booksRouter);

server.listen(3000, () => {
  console.log('Server running on port 3000');
});

// Missään ei nyt tarkisteta, että käyttäjällä on token kun hän yrittää käyttää books endpointeja.
// Tehtävänanto: "All book routes should be secured to require a bearer token to access them. On any error a response with appropriate status code and error message should be sent."
