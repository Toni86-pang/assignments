import express, { Request, Response } from 'express';
import argon2 from 'argon2';
import jwt from 'jsonwebtoken';
import dotenv from 'dotenv';

dotenv.config();

const usersRouter = express.Router();

interface User {
  username: string;
  password: string;
}

const users: User[] = [];

usersRouter.post('/register', async (req: Request, res: Response) => {
  const { username, password } = req.body;
  const existingUser = users.find((user) => user.username === username);
  if (existingUser) { // TS:ssä eksplisiittisesti, existingUser !== undefined
    console.log('Registration failed: Username already exists');
    return res.status(500).send();
  }

  try {
    const hash = await argon2.hash(password);
    const newUser: User = { username, password: hash };
    users.push(newUser);
    // mielummin users = users.concat(newUser)
    console.log('User registered:', newUser);

    const token = jwt.sign({ username: newUser.username }, process.env.SECRET || '', {
      expiresIn: '15m',
    });

    res.status(201).json({ token });
  } catch (error) {
    console.error('Error during password hashing:', error);
    res.status(500).send();
  }
});

usersRouter.post('/login', (req: Request, res: Response) => {
  const { username, password } = req.body;
  const user = users.find((user) => user.username === username);

  if (!user) {
    return res.status(401).send();
  }

  argon2
    .verify(user.password, password)
    .then((passwordMatches) => {
      if (passwordMatches) {
        const token = jwt.sign({ username: user.username }, process.env.SECRET || '', {
          expiresIn: '15m',
        });
        return res.status(200).json({ token });
      } else {
        return res.status(401).send();
      }
    })
    .catch((error) => {
      console.error('Error during password verification:', error);
      return res.status(500).send();
    });
});

export default usersRouter;
