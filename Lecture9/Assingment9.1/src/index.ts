import express from 'express';
import router from './studentRouter';

const server = express();


server.use(express.static('public'));


server.use(express.json());


server.use('/', router);

const port = 3000;
server.listen(port, () => {
    console.log('Server listening on port', port);
});
