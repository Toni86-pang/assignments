import express, { Request, Response } from 'express';
import argon2 from 'argon2';
import jwt from 'jsonwebtoken';
import dotenv from 'dotenv';

dotenv.config();
const server = express();

server.use(express.json());

interface User {
  username: string;
  password: string;
}

interface CustomRequest extends Request {
  user?: User;
}

const users: User[] = [];

server.get('/', (req, res) => {
  res.send('Hello World');
});

server.post('/register', async (req: CustomRequest, res: Response) => {
    const { username, password } = req.body;
    const existingUser = users.find((user) => user.username === username);
    if (existingUser) {
      console.log('Registration failed: Username already exists');
      return res.status(500).send();
    }
  
    try {
      const hash = await argon2.hash(password);
      const newUser: User = { username, password: hash };
      users.push(newUser);
      console.log('User registered:', newUser);
      res.status(201).json(newUser);
    } catch (error) {
      console.error('Error during password hashing:', error);
      res.status(500).send();
    }
  });
  
server.post('/login', (req: CustomRequest, res: Response) => {
    const { username, password } = req.body;
    const user = users.find((user) => user.username === username);
  
    if (!user) {
      return res.status(401).send();
    }
  
    argon2
      .verify(user.password, password)
      .then((passwordMatches) => {
        if (passwordMatches) {
          const token = jwt.sign({ username: user.username }, process.env.SECRET || '', {
            expiresIn: '15m',
          });
          return res.status(200).json({ token });
        } else {
          return res.status(401).send();
        }
      })
      .catch((error) => {
        console.error('Error during password verification:', error);
        return res.status(500).send();
      });
  });
  
export default server;
