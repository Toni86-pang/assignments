import request from 'supertest';
import server from '../src/server'; 

describe('User API', () => {
    describe('POST /register', () => {
      test('should register a new user', async () => {
        const response = await request(server)
          .post('/register')
          .send({ username: 'newuser', password: 'newpassword' });
        expect(response.status).toBe(201);
      });
  
      test('should return an error if registration fails', async () => {
        const response = await request(server)
          .post('/register')
          .send({ username: 'testuser', password: 'testpassword' });
  
        expect(response.status).toBe(201); 
      });
      // meneekö nää testit läpi? molemmissa vaaditaan 201
    });


  describe('POST /login', () => {
    test('should login an existing user', async () => {
      const response = await request(server)
        .post('/login')
        .send({ username: 'testuser', password: 'testpassword' });
      expect(response.status).toBe(200);
      // tässä vois tarkastaa että response.body sisältää tokenin
    });
  
    test('should return an error if login fails', async () => {
      const response = await request(server)
        .post('/login')
        .send({ username: 'testuser', password: 'incorrectpassword' });
      
      expect(response.status).toBe(401);
    });
  });
});
