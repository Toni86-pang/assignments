import express, { Request, Response } from 'express';
import argon2 from 'argon2';
import jwt from 'jsonwebtoken';
import dotenv from 'dotenv';

dotenv.config();

interface User {
  username: string,
  password: string,
  isAdmin?: boolean
}

let users: Array<User> = [];
const secret = process.env.SECRET

export const usersRouter = express.Router();


usersRouter.post('/register', async (req: Request, res: Response) => {
  
  const { username, password } = req.body;
  console.log('tässä?')
  const existingUser = users.find(user => user.username === username);
  
  if (existingUser !== undefined) {
    return res.status(400).send('Registration failed: Username already exists');
  }
if (username === process.env.ADMIN_USER ){
  return res.status(400).send('Username is invalid')
}
  
    const hash = await argon2.hash(password);
    const newUser: User = { 
      username,
       password
      }
      console.log('täällä') // debug-loggaukset pois lopullisesta versiosta
    users.push(newUser);
    console.log('User registered:', newUser, hash);

    const token = jwt.sign( {username}, process.env.SECRET || '', {expiresIn: '1h'});
    res.send(token);
  });

usersRouter.post('/login', async (req: Request, res: Response) => {
  const { username, password } = req.body;
  const user = users.find((user) => user.username === username);
  
  if(username === process.env.ADMIND_USER){
    const adminHash = process.env.ADMIN_HASH ?? ''
    const isValidAdminPassword = await argon2.verify(adminHash, password)
    
    if(!isValidAdminPassword){
      return res.status(401).send('incorrect username or password')
    }
   
    const token = jwt.sign({username, isAdmin: true}, process.env.SECRET || '', {expiresIn:'1h'})
    return res.send({token})
  }

  if (user === undefined) {
    return res.status(401).send('Incorrect username or password');
  }

  argon2
    .verify(user.password, password)
    .then((passwordMatches) => {
      if (passwordMatches) {
        const token = jwt.sign({ username}, process.env.SECRET || '', { expiresIn: '15m',});
        return res.status(200).json({ token });
      } else {
        return res.status(401).send('Incorrect username or password');
      }
    })
    .catch((error) => {
      console.error('Error during password verification:', error);
      return res.status(500).send();
    });
});

export default usersRouter;
