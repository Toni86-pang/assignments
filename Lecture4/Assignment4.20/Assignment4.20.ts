
const startFrom = Number(process.argv[2]);
const stopHere = Number(process.argv[3]);

const range = (startFrom: number, stopHere: number): number[] => {
    return Array.from({ length: stopHere - startFrom + 1 }, (_, i) => startFrom + i);
};

const result: number[] = range(startFrom, stopHere);
console.log(result);
