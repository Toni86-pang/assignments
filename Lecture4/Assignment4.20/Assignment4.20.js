"use strict";

const startFrom = Number(process.argv[2]);
const stopHere = Number(process.argv[3]);
const range = (startFrom, stopHere) => Array.from({ length: stopHere - startFrom + 1 }, (_, i) => startFrom + i);
range(startFrom, stopHere);
