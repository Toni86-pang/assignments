"use strict";
function longSum(callback) {
    let result = 0;
    for (let i = 0; i < 1000000; i++) {
        if (i % 3 === 0 && i % 5 === 0 && i % 7 === 0) {
            result += i;
        }
    }
    callback(result);
}
function logger(aThingToLog) {
    console.log("logger says");
    console.log(aThingToLog);
}
longSum(logger);
