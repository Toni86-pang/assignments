
function longSum(callback: (result: number) => void): void {
    let result = 0;
    for (let i = 0; i < 1000000; i++) {
        if (i % 3 === 0 && i % 5 === 0 && i % 7 === 0) {
            result += i;
        }
    }
    callback(result);
}
  
function logger(aThingToLog: number): void {
    console.log("logger says");
    console.log(aThingToLog);
}
  
longSum(logger);
  