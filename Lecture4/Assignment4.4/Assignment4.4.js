"use strict";
const dice = () => {
    const sides = (number) => {
        const random = Math.floor(Math.random() * number) + 1;
        return random;
    };
    return sides;
};
const diceSix = dice();
const diceEight = dice();
const damage = diceSix(6) + diceSix(6) + diceEight(8) + diceEight(8);
console.log(damage);
