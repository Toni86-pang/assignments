"use strict";
const palindrome = (process.argv[2]);
function checkPalindrome(text) {
    const reverseString = text.split("").reverse().join("");
    if (text == reverseString) {
        console.log("It is a palindrome");
    }
    else {
        console.log("It is not a palindrome");
    }
}
checkPalindrome(palindrome);
