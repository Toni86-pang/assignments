
const palindrome: string = process.argv[2];

function checkPalindrome(text: string): void {
    const reverseString: string = text.split("").reverse().join("");
    if (text === reverseString) {
        console.log("It is a palindrome");
    } else {
        console.log("It is not a palindrome");
    }
}

checkPalindrome(palindrome);
