"use strict";

const names = [
    "rauni",
    "matias",
    "Kimmo",
    "Heimo",
    "isko",
    "Sulevi",
    "Mikko",
    "daavid",
    "otso",
    "herkko"
];

names.forEach(function (sift) {
    const str2 = sift.charAt(0).toUpperCase() + sift.slice(1);
    console.log(str2);
});
