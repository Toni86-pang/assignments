"use strict";
const numbers = [1, 2, 3, 4, 5, 6, 7,
    8, 9, 10, 11, 12, 13, 14, 15, 16, 17,
    18, 19, 20, 21, 22];
//jaettu 
const divisible3 = numbers.filter((a) => a % 3 === 0);
console.log(divisible3);
//kerrottu
const Multiply2 = numbers.map(acc => acc * 2);
console.log(Multiply2);
//summa
const sum = numbers.reduce((acc, cur) => acc + cur, 0);
console.log(sum);
