
const numbers = [
    749385,
    498654,
    234534,
    345467,
    956876,
    365457,
    235667,
    464534,
    346436,
    873453
];

function divisionFilter(numbers: Array<number>) {
    return numbers.filter(number => {
        if (number % 5 === 0 && number % 3 === 0) return false;
        if (number % 5 === 0 || number % 3 === 0) return true;
        return false;
    });
}

console.log(divisionFilter(numbers));
