
const numb = Number(process.argv[2]);
let prime = true;

if (numb === 1) {
    console.log("Number one can't be a prime number");
} else if (numb > 1) {
    for (let i = 2; i < numb; i++) {
        if (numb % i === 0) {
            prime = false;
            break;
        }
    }

    if (prime) {
        console.log(`${numb} is a prime number`);
    } else {
        console.log(`${numb} is not a prime number`);
    }
} else {
    console.log("The number is not a prime number.");
}
