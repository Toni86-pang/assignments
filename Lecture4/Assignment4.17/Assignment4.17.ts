
const str: string = process.argv[2];

function reverseString(string: string): string {
    return string
        .split("")
        .reverse()
        .join("")
        .split(" ")
        .reverse()
        .join(" ");
}

console.log(reverseString(str));
