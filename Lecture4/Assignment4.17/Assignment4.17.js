"use strict";
const str = (process.argv[2]);
function reverseString(string) {
    return string.split("").reverse().join("").split(" ").reverse().join(" ");
}
console.log(reverseString(str));
