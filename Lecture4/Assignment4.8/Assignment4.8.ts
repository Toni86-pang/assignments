
const words = [
    "city",
    "distribute",
    "battlefield",
    "relationship",
    "spread",
    "orchestra",
    "directory",
    "copy",
    "raise",
    "ice"
];
const reverse = (word) => {
    return word.split("").reverse().join("");
};
function reverseList(list) {
    return list.map(word => reverse(word));
}
console.log(reverseList(words));
