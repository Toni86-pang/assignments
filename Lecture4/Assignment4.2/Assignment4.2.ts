
function calcFunction(min: number, max: number): number {
    const range: number = max - min;
    let rand: number = Math.random() * range + min;
    rand = Math.floor(rand);
    return rand;
}
  
for (let i = 0; i < 20; i++) {
    const r: number = calcFunction(60, 100);
    console.log(r);
}
  