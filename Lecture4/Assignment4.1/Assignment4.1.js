"use strict";
function epicTitleLogger(part1 = "hermanni") {
    const words = part1.split("");
    let finalProduct = "";
    for (let i = 0; i < words.length; i++) {
        const word = words[i];
        finalProduct = finalProduct + word[0].toUpperCase() + word.substring(1) + " ";
    }
    return finalProduct.trim();
}
console.log(epicTitleLogger());
