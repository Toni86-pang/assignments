function epicTitleLogger(part1 = "hermanni"): string {
    const words: string[] = part1.split("");
    let finalProduct = "";
  
    for (let i = 0; i < words.length; i++) {
        const word: string = words[i];
        finalProduct = finalProduct + word[0].toUpperCase() + word.substring(1) + " ";
    }
  
    return finalProduct.trim();
}
  
console.log(epicTitleLogger());
  