
const names = [
    'Murphy',
    'Hayden',
    'Parker',
    'Arden',
    'George',
    'Andie',
    'Ray',
    'Storm',
    'Tyler',
    'Pat',
    'Keegan',
    'Carroll'
];
const doJoin = (string, separator) => {
    return string.reduce((acc, cur) => acc + separator + cur);
};
console.log(doJoin(names, "Teamblue"));
