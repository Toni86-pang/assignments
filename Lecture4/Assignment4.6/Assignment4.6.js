"use strict";
const factorial = (n) => {
    if (n === 1) {
        return n;
    }
    else {
        return n * factorial(n - 1);
    }
};
const n = 4;
console.log(factorial(n));
