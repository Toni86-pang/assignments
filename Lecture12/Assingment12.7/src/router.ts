import express, { Request, Response } from 'express'
import {addProduct, findOneProduct, findAllProducts, deleteProduct, updateProduct} from './productdao'

const router = express.Router()

router.post('/', async (req:Request, res:Response) => {
    const {id,name , price} = req.body
    const result = await addProduct (name ,price)
    res.send({id, name , price})
    })
    console.log('producet added to database')

    router.get('/', async (req: Request, res: Response) => {
        const products = await findAllProducts();
        res.send(products);
    })

router.get('/:id', async (req:Request, res:Response) => {
    const {id} = req.params
    const parseId = parseInt(id, 10)
    const product = await findOneProduct(parseId)
    
    if(product){
    res.send(product)
    }
    else {
        console.log('nope no products here ')
    }
})

router.put('/:id', async (req:Request, res:Response) => {
    const {id} = req.params
    const {name, price} = req.body
    await updateProduct(parseInt(id, 10), name , price)
    res.send({id , name ,price})
})

router.delete('/:id', async (req:Request, res:Response) => {
    const {id} = req.params
    await deleteProduct(parseInt(id, 10))
    res.send('it´s gone jimmy!')
})



export default router