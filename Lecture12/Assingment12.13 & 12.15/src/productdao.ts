import { executeQuery } from "./db";


/*       Users osio       */
export const addUser = async (username:string , full_name:string, email:string) =>{
const query = `INSERT INTO users 
(username,full_name, email) 
VALUES ($1 , $2, $3) 
RETURNING id`
const params = [username,full_name, email]
const result = await executeQuery(query, params)
console.log('added user \n', result.rows[0].id)
};

export const findAllUsers = async () => {
    const query = `SELECT 
    id , username 
    FROM users`
    const result = await executeQuery(query);
    const users = result.rows; 
    console.log("All users found \n", users);
    return users;
  };

  
export const findOneUser = async (id:number) =>{
    const query = `SELECT 
    id , username , 
    full_name, email 
    FROM users WHERE id = $1`
    const params = [id]
    const result = await executeQuery(query, params)
    return result.rows[0];
};

export const deleteUsers = async (id:number) =>{
  const query = 'DELETE FROM users WHERE id = $1'
  const params = [id]
  const result = await executeQuery(query, params)
  return result
};


/*              Posts osio               */
export const addPost = async (title:string , content:string) =>{
  const query = `INSERT INTO posts 
  (title,content,post_date) 
  VALUES ($1 , $2, NOW()) 
  RETURNING id_user_post`
  const params = [title , content]
  const result = await executeQuery(query, params)
  console.log('post added \n', result.rows[0].id)
  };
  
export const findUserPosts = async (id_user_post:number) => {
      const query = `SELECT 
      posts.id_user_post, 
      posts.title, 
      posts.content,
      posts.post_date, 
      comment.id_user_comment,
      comment.comment_post_title, 
      comment.comment_content,
      comment.comment_date
      FROM posts 
      JOIN comment ON posts.id_user_post = comment.id_user_comment
      WHERE posts.id_user_post = $1`
      const params = [id_user_post]
      const result = await executeQuery(query, params);
      const posts = result.rows[0]; 
      return posts;
    };
  
export const findAllUserPosts = async (): Promise<any> =>{
      const query = `SELECT 
      id_user_post, title 
      FROM posts `
      const result = await executeQuery(query)
      const posts = result.rows
        return posts
  };

  export const deletePosts = async (id_user_post:number) =>{
    const query = 'DELETE FROM posts WHERE id_user_post = $1'
    const params = [id_user_post]
    const result = await executeQuery(query, params)
    return result
};


  /*         Comments osio           */
export const addComment = async (comment_content:string , comment_post_title:string) =>{
    const query = `INSERT INTO comment 
    (comment_post_title, comment_content, comment_date) 
    VALUES ($1 , $2, NOW())
    RETURNING id_user_comment`
    const params = [comment_content,comment_post_title]
    const result = await executeQuery(query, params)
    console.log('comment added \n', result.rows[0].id)
    };
    
export const findAllUserComments = async (id_user_comment:number) =>{
        const query = `SELECT 
        id_user_comment , 
        comment_post_title,
        comment_content 
        FROM comment WHERE id_user_comment = $1`
        const params = [id_user_comment]
        const result = await executeQuery(query, params)
        const comment = result.rows[0]
        return comment
    };
  

export const deleteComments = async (id_user_comment:number) =>{
       const query = 'DELETE FROM comment WHERE id_user_comment = $1'
       const params = [id_user_comment]
       const result = await executeQuery(query, params)
       return result
};
