import express, { Request, Response } from 'express'
import {addUser, findOneUser, findAllUsers,
        addPost, findUserPosts, findAllUserPosts,
        addComment, findAllUserComments,
        deleteComments,deletePosts,deleteUsers
        } from './productdao'

const router = express.Router()


/*            users osio                  */ 
router.post('/users', async (req:Request, res:Response) => {
    const {id ,username , full_name, email} = req.body
    const result = await addUser (username , full_name , email)
    res.send({id , username , full_name , email})
})

router.get('/users', async (req: Request, res: Response) => {
    const users = await findAllUsers();
    res.send(users);
})

router.get('/users/:id', async (req:Request, res:Response) => {
    const {id} = req.params
    const parseId = parseInt(id, 10)
    const users = await findOneUser(parseId)
    res.send(users)
})

router.delete('/users/:id', async (req:Request, res:Response) => {
    const {id} = req.params
    await deleteUsers(parseInt(id, 10))
    res.send('going to gone jimmy!')
})

/*              post osio                   */
router.post('/posts', async (req:Request, res:Response) => {
    const {id,title , content} = req.body
    const result = await addPost (title ,content)
    res.send({id, title , content})
})
   
router.get('/posts', async (req: Request, res: Response) => {
    const posts = await findAllUserPosts()
    res.send(posts);
})

router.get('/posts/:id', async (req:Request, res:Response) => {
    const {id} = req.params
    const parseId = parseInt(id, 10)
    const posts = await findUserPosts(parseId)
    res.send(posts)
})

router.delete('/posts/:id', async (req:Request, res:Response) => {
    const {id} = req.params
    await deletePosts(parseInt(id, 10))
    res.send('it´s gone jimmy!')
})

/*                comment osio                            */
router.post('/comment', async (req:Request, res:Response) => {
    const {title, content} = req.body
    const result = await addComment (title,content)
    res.send({title, content})
})
   

router.get('/comments/:id', async (req: Request, res: Response) => {
    const {id} = req.params
    const parseId = parseInt(id, 10)
    const comment = await findAllUserComments(parseId)
    res.send(comment)
})


router.delete('/comments/:id', async (req:Request, res:Response) => {
    const {id} = req.params
    await deleteComments(parseInt(id, 10))
    res.send('not anymore jimmy!')
})

export default router