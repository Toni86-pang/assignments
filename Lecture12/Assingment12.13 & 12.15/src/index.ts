import express from 'express'
//import { createProductsTable } from './db'
import router from './router'

const server = express()
server.use(express.json());
server.use(router)



const { PORT } = process.env
server.listen(PORT, () => {
    console.log('Forum API listening to port', PORT)
})
