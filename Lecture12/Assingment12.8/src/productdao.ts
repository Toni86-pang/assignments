import { executeQuery } from "./db";

export const addProduct = async (name:string , price:number) =>{
const query = 'INSERT INTO products (name,price) VALUES ($1 , $2) RETURNING id'
const params = [name,price]
const result = await executeQuery(query, params)
console.log('product added \n', result.rows[0].id)
};


export const findAllProducts = async () => {
    const query = 'SELECT * FROM products';
    const result = await executeQuery(query);
    const products = result.rows; // Haetaan tuotteet rows-ominaisuuden sisältä
    console.log("All products found \n", products);
    return products;
  };

export const findOneProduct = async (id:number) =>{
    const query = 'SELECT id , name , price FROM products WHERE id = $1'
    const params = [id]
    const result = await executeQuery(query, params)

if(result.rows.length > 0){
const {id , name , price} = result.rows[0]
    console.log('product found \n', id , name , price)
    return {id, name , price}
}
else {
    return null ;
  }
};

export const deleteProduct = async (id:number) =>{
       const query = 'DELETE FROM products WHERE id = $1'
       const params = [id]
       const result = await executeQuery(query, params)
       return result
};
    
export const updateProduct = async (id: number, name:string, price: number) => {
    const query = 'UPDATE products SET name = $2, price = $3 WHERE id = $1'
    const params = [id, name, price]
    const result = await executeQuery(query, params)
    console.log('product updated \n', result)
    
};
