import  { useState } from "react";


type Player = "X" | "O";
type Board = Player | null;

const initialBoard: Board[] = Array(9).fill(null);

const calculateWinner = (board: Board[]): Player | null => {
  // Winning combinations
  const lines = [
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],
    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8],
    [0, 4, 8],
    [2, 4, 6],
  ];

  for (const line of lines) {
    const [a, b, c] = line;
    if (board[a] && board[a] === board[b] && board[a] === board[c]) {
      return board[a] as Player;
    }
  }

  return null;
};

const TicTacToe = () => {
  const [board, setBoard] = useState<Board[]>(initialBoard);
  const [currentPlayer, setCurrentPlayer] = useState<Player>("X");
  const [winner, setWinner] = useState<Player | null>(null);

  const handleClick = (index: number) => {
    if (board[index] || winner) {
      return;
    }

    const updatedBoard = [...board];
    updatedBoard[index] = currentPlayer;
    setBoard(updatedBoard);

    const newWinner = calculateWinner(updatedBoard);
    if (newWinner) {
      setWinner(newWinner);
    } else {
      setCurrentPlayer(currentPlayer === "X" ? "O" : "X");
    }
  };

  const handleReset = () => {
    setBoard(initialBoard);
    setCurrentPlayer("X");
    setWinner(null);
  };

  const renderBoard = () => {
    return (
      <div className="board">
        {[0, 1, 2].map((col) => (
          <div className="col" key={col}>
            {[0, 1, 2].map((row) => {
              const index = col * 3 + row;
              return (
                <button
                  className="square"
                  key={index}
                  onClick={() => handleClick(index)}
                >
                  {board[index]}
                </button>
              );
            })}
            {/* Saattais olla simppelimpää vaan hard-codata 9:n mittanen array laudaksi, kuin tehdä kaks sisäkkäistä kolmen mittasta looppia */}
          </div>
        ))}
      </div>
    );
  };

  let status;
  if (winner) {
    status = `Player ${winner} has won!`;
  } else if (board.every((square) => square !== null)) {
    status = "It's a tie!";
  } else {
    status = `Current Player: ${currentPlayer}`;
  }

  return (
    <div className="container">
      <div className="game">
        <h2>Tic Tac Toe</h2>
        {renderBoard()}
        <div className="status">{status}</div>
        <button className="reset-button" onClick={handleReset}>
          Reset
        </button>
      </div>
    </div>
  );
};

export default TicTacToe;


// Siistin näköistä koodia tämäkin. CSS-asetteluissa on jotain jännittävää, koska lauta ei pysy pelatessa neliön muotoisena.

// Geneerisesti: assets-kansioon siis ei-koodillisia resursseja. tsx-tiedostot voit laittaa suoraan src alle. 
// App-komponentti ei ole millään tapaa pakollinen. Jos haluat, voit käyttää suoraan TicTacToe komponenttia mainista ja poistaa koko App-komponentin.