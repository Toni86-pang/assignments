import { useState } from 'react'

function Numbers() {
    const [number1, setNumber1] = useState(0)
    const [number2, setNumber2] = useState(0)
    const [number3, setNumber3] = useState(0)

    const click1 = () => {
        setNumber1( number1 + 1)
    }
    const click2 = () => {
        setNumber2( number2 +1)
    }
    const click3 = () => {
        setNumber3( number3 +1)
    }

    return (
        <div className='buttons'>
            <button onClick={click1}>
            {number1}
            </button>
            <br/>
            <button onClick={click2}>
            {number2}
            </button>
            <br/>
            <button onClick={click3}>
            {number3}
            </button>
            <br/>
            <h1>{number1 + number2 + number3}</h1>
            <br />
        </div>
    )
}

export default Numbers