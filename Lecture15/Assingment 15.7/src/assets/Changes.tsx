import { useState, ChangeEvent } from 'react'

function Changes() {
  const [myText, setMyText] = useState('')
const [myTitle, setMyTitle] = useState('') 

const onClickChange =() =>{
  setMyTitle(myText)
}

const onInputChange = (event: ChangeEvent<HTMLInputElement>) => {
    setMyText(event.target.value)
}

  return (
    <div className='App'>
      <h1>Your string is:{myTitle}</h1>
      <input type='text'
        value={myText} 
        onChange={onInputChange}/>
        <button onClick={onClickChange}>submit</button>
      </div>
  )
}

export default Changes