import './App.css'
import Changes from './assets/Changes'


function App() {
  return (
  <div className='App'>
       <Changes/>
  </div>
    )
}

// Tää on turha tiedosto, jos siinä ei tehä muuta kuin oteta yks komponentti käyttöön. Voit ottaa Changes käyttöön suoraan mainissa tai laittaa Changes koodin App-komponenttiin.

export default App
