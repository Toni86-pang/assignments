import { useState } from "react"


interface TodoItem {
    text: string
    isDone: boolean
}

function Todo() {
    const [todo, setTodo] = useState<string>('')
    const [todos, setTodos] = useState<TodoItem[]>([])


    const addTodo = () => {
        if (todo !== '') {
            setTodos([...todos, { text: todo, isDone: false }])
            setTodo('')
        }
    }

    const deleteTodo = (text: string) => {
        const newTodos = todos.filter((todo) => {
            return todo.text !== text
        })
        setTodos(newTodos)
    }


    const toggleTodo = (text: string) => {
        const updateTodos = todos.map((todo) => {
            if (todo.text === text) {
                return { ...todo, isDone: !todo.isDone }
            }
            return todo
        })
        setTodos(updateTodos)
    }

    // Mitä tapahtuu, jos meillä on kaks saman nimistä todoa?

    return (

        <div className='todo'>
            <h1> Tonin todo app </h1>

            <div className='input container' >
                <input
                    type='text'
                    name='todo'
                    value={todo}
                    placeholder='add new thing todo'
                    onChange={(event) => { setTodo(event.target.value) }}
                />
                <button className='add button' onClick={addTodo} >add todo</button>
            </div>




            {todos?.length > 0 ? (
                <ul className='todo-list'>
                    {todos.map((todo, index) => (
                        <div className='todo' key={index} >
                            <li>{todo.text}</li>
                            <input
                                className='checkbox'
                                type='checkbox'
                                checked={todo.isDone}
                                onChange={() => toggleTodo(todo.text)}
                            />
                            <button
                                className='delete-button'
                                onClick={() => {
                                    deleteTodo(todo.text)
                                }}>
                                Delete</button>
                        </div>
                    ))}
                </ul>
            ) : (
                <div className='empty'>
                    <p> no tasks in here</p>
                </div>
            )}
        </div>
    )
}
export default Todo

// Selkeä tiedosto, helppo lukea ja ymmärtää