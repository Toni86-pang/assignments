import './App.css'
import Peoples from './assets/Persons'

const nameList = ["Ari", "Jari", "Kari", "Sari", "Mari", "Sakari", "Jouko"]

function App() {
    return (
        <div className='App'>
            <h1>Great names</h1>
            <Peoples names={nameList} />
        </div>
    )
}

export default App
