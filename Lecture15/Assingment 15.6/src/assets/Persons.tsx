

interface Props {
    names: Array<string>
}

function Peoples(props: Props) {
    return (
        <ul>
            {props.names.map((person, i) => {
                return i % 2 === 0
                    ? <li><b key={'names' + i}>{person}</b></li>
                    : <li><i key={'names' + i}>{person}</i></li>
            }
            )}
        </ul>
    )
}
export default Peoples