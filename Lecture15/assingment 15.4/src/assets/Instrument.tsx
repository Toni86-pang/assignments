
interface Props {
    pic: string
    name: string
    price: number
}

const Instrument = (props: Props) => {
    return (
      <div className='UserProfile'>
        <h1>{props.name}</h1>
        <h3>{props.price}</h3>
        <img src={props.pic}/>
        

      </div>
    )
  }


export default Instrument