import Instrument from './assets/Instrument'
import './App.css'
import guitar from './assets/images/guitar.png'
import violin from './assets/images/violin.png'
import tuba from './assets/images/tuba.png'
// niinku näin :D


function App() {

    return (
        <>
            <h1 > Great instruments</h1>
            <div className='App'>
                <Instrument name='Tuuba' price={1000} pic={tuba} />
                <Instrument name='Viulu' price={500} pic={violin} />
                <Instrument name='Kitara' price={200} pic={guitar} />
            </div>
        </>
    )
}

export default App
