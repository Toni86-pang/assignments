import express, { Request, Response} from 'express';
import { middleware, unknownError } from './middleware';

const server = express();

const students = [

];

server.use(middleware);
server.get('/student', (req: Request, res: Response) => {
  res.send(students);
});
server.use(unknownError);

server.listen(3000);
