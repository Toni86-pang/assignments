import { Request, Response, NextFunction} from 'express';
const middleware = (req: Request, res: Response, next: NextFunction) => {
	const time = new Date ();
	const logger = {
                  url: req.url,
                  time: time.getDate(), 
                  metod : req.method,
                  };
  
	console.log(logger);
	next();
};


const unknownError = (_req, res) => {
  res.status(404).send('Wrong Params')
} 


export{middleware, unknownError};