import express, { Request, Response, NextFunction} from 'express';

const students = [

]; // tarpeeton tyhjä rivi, kannattaa tarkastaa ettei eslint tee näitä outoja muotoiluja

const server = express();

const middleware = (req: Request, res: Response, next: NextFunction) => {
	const url = req.url;
	const time = new Date ();
	const metod = req.method;
	const logger = {url : url , time : time , metod : metod};
	console.log(logger);
	next();
};

server.use(middleware);

server.get('/student', (req: Request, res: Response) => {
	res.send(students);
});

server.listen(3000);