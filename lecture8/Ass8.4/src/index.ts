import express, {Request, Response } from 'express'

const server = express()
let counter = 0 // käyttämätön muuttuja
const data = [
  { counter: 0, name: 'Aaron' },
  { counter: 0, name: 'Bill' }
]


server.get('/counter/:name', (req: Request, res: Response) => {
    const name = req.params.name
    const info = data.find(item => item.name === name)

    if(info.name === 'Aaron') {
      data[0].counter += 1
      res.send(`Aaron eas here ${data[0].counter}`)
    } else {
      data[1].counter += 1
      res.send(`Bill eas here ${data[1].counter}`)
    }
    // Tässä on ehkä oikastu hieman. Tehtävän tarkotus oli soveltaa tuota 'name' parametria ja jonka mukaan laskea käyntejä.
    // Eli kun käyttäjä navigoi "/counter/Aaron" niin tieto käynneistä lisättäisiin Aaron nimiselle vierailijalles. 
    
})

server.listen(3000)

