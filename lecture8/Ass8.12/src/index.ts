import express from 'express';
import helmet from 'helmet';
import { logger, validateBookData, errorHandler } from './middleware';

const server = express();
server.use(express.json());
server.use(helmet());

interface Book {
  id: number;
  name: string;
  author: string;
  read: boolean;
}

const books: Book[] = [];

server.use(logger);

server.get('/api/v1/books', (_req, res) => {
	res.json(books);
});

server.get('/api/v1/books/:id', (req, res) => {
	const id = Number(req.params.id);
	const book = books.find((book) => book.id === id);

	if (book) {
		res.json(book);
	} else {
		res.status(404).send('Book not found');
	}
});

server.post('/api/v1/books', validateBookData, (req, res) => {
	const { id, name, author, read } = req.body;

	const newBook: Book = {
		id: Number(id),
		name: String(name),
		author: String(author),
		read: Boolean(read),
	};

	books.push(newBook);
	res.status(201).send('Book created');
});

server.put('/api/v1/books/:id', validateBookData, (req, res) => {
	const id = Number(req.params.id);
	const bookIndex = books.findIndex((book) => book.id === id); // Tämä findIndex ratkasu näyttää vähän purkkaiselta. Tähän suosittelen filter() metodia

	if (bookIndex === -1) {
		res.status(404).send('Book not found');
	} else {
		const { name, author, read } = req.body;

		const updatedBook: Book = {
			id: id,
			name: String(name),
			author: String(author),
			read: Boolean(read),
		};

		books[bookIndex] = updatedBook;
		res.status(204).send();
	}
});

server.delete('/api/v1/books/:id', (req, res) => {
	const id = Number(req.params.id);
	const bookIndex = books.findIndex((book) => book.id === id); // Tämä findIndex ratkasu näyttää vähän purkkaiselta. Tähän suosittelen filter() metodia

	if (bookIndex === -1) {
		res.status(404).send('Book not found');
	} else {
		books.splice(bookIndex, 1);
		res.status(204).send();
	}
});

server.use(errorHandler);

server.listen(3000, () => {
	console.log('Server running on port 3000');
});
