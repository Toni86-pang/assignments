import express, { Request, Response } from 'express';

const server = express();
server.use(express.json());

interface Book {
  id: number;
  name: string;
  author: string;
  read: boolean;
}

const books: Book[] = [];

server.get('/api/v1/books', (_req: Request, res: Response) => {
	res.json(books);
});

server.get('/api/v1/books/:id', (req: Request, res: Response) => {
	const id = Number(req.params.id);
	const book = books.find((book) => book.id === id);

	if (book) {
		res.json(book);
	} else {
		res.status(404).send('Book not found');
	}
});

server.post('/api/v1/books', (req: Request, res: Response) => {
	const { id, name, author, read } = req.body;

	if (!id || !name || !author || read === undefined) {
		res.status(400).send('Invalid book data');
	} else {
		const newBook: Book = {
			id: Number(id),
			name: String(name),
			author: String(author),
			read: Boolean(read),
		};

		books.push(newBook);
		res.status(201).send('Book created');
	}
});

server.put('/api/v1/books/:id', (req: Request, res: Response) => {
	const id = Number(req.params.id);
	const bookIndex = books.findIndex((book) => book.id === id);

	if (bookIndex === -1) {
		res.status(404).send('Book not found');
	} else {
		const { name, author, read } = req.body;

		if (!name || !author || read === undefined) {
			res.status(400).send('Invalid book data');
		} else {
			const updatedBook: Book = {
				id: id,
				name: String(name),
				author: String(author),
				read: Boolean(read),
			};

			books[bookIndex] = updatedBook;
			res.status(204).send();
		}
	}
});

server.delete('/api/v1/books/:id', (req: Request, res: Response) => {
	const id = Number(req.params.id);
	const bookIndex = books.findIndex((book) => book.id === id); // Tämä findIndex ratkasu näyttää vähän purkkaiselta. Tähän suosittelen filter() metodia

	if (bookIndex === -1) {
		res.status(404).send('Book not found');
	} else {
		books.splice(bookIndex, 1);
		res.status(204).send();
	}
});

server.listen(3000, () => {
	console.log('Server running on port 3000');
});