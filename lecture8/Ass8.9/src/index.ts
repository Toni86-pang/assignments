import express, { Request, Response } from 'express';
import { middleware, unknownError } from './middleware';

const server = express();
server.use(express.json());

const students: Student[] = [];

server.use(middleware);

interface Student {
  id: string;
  name: string;
  email: string;
}

server.post('/student', (req: Request, res: Response) => {
	if (!req.body.id || !req.body.name || !req.body.email) {
		console.log('error 400');
		res.status(400).send('400 error lol');
	} else {
		const student: Student = {
			id: req.body.id,
			name: req.body.name,
			email: req.body.email,
		};
		students.push(student);
		res.status(201).send();
	}
});

server.get('/student/:id', (req: Request, res: Response) => {
	const id = req.params.id;
	const student = students.find((student) => student.id === id);

	if (student) {
		res.send(student);
	} else {
		res.status(404).send('Cant find dat ass');
	}
});

server.get('/student', (req: Request, res: Response) => {
	const studenIds = students.map((ele) => ele.id);
	res.send(studenIds);
});

server.put('/student/:id', (req: Request, res: Response) => {
	const id = req.params.id;
	const student = students.find((student) => student.id === id);

	if (!student) {
		res.status(404).send('Student not found');
	} else {
		if (req.body.name) {
			student.name = req.body.name;
		}
		if (req.body.email) {
			student.email = req.body.email;
		}
		res.sendStatus(204);
	}
});

server.delete('/student/:id', (req: Request, res: Response) => {
	const id = req.params.id;
	const index = students.findIndex((student) => student.id === id); // Tämä findIndex ratkasu näyttää vähän purkkaiselta. Tähän suosittelen filter() metodia

	if (index === -1) {
		res.status(404).send('Student not found');
	} else {
		students.splice(index, 1);
		res.sendStatus(204);
	}
});

server.use(unknownError);

server.listen(3000, () => {
	console.log('Server running on port 3000');
});
