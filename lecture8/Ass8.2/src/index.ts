import express, {Request, Response } from 'express';

const server = express();
const plus = 5; // käyttämätön muuttuja
const counter = 0;

server.get('/counter', (req: Request, res: Response) => {
    const number = Number(req.query.number);

    number? res.send({counter:number, number:number}) 
    
        :   res.send({counter:counter, number:number}); // tässä on nyt vissiin eslint formatoinut ternary operaation vähän hassusti?
     
});

server.listen(3000);
