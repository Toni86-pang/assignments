import express, { Request, Response, NextFunction } from 'express';

const server = express();
server.use(express.json());

interface Student {
  name: string;
  score: number;
}

const validateInput = (req: Request, res: Response, next: NextFunction) => {
	const students: Student[] = req.body;

	if (!Array.isArray(students) || students.length === 0) {
		return res.status(400).json({ error: 'something went wrong' });
	}

	for (const student of students) {
		if (
			typeof student.name !== 'string' || typeof student.score !== 'number') {
			return res.status(400).json({ error: 'Invalid input' });
		}
	}

	next();
};

const calculateAverageScore = (students: Student[]): number => {
	const totalScore = students.reduce((sum, student) => sum + student.score, 0);
	return totalScore / students.length;
};

const findHighestScoringStudent = (students: Student[]): Student => {
	return students.reduce((highest, current) => {
		return current.score > highest.score ? current : highest;
	});
};

const calculatePercentageAboveAverage = (
	students: Student[],
	average: number
): number => {
	const aboveAverage = students.filter((student) => student.score >= average);
	return (aboveAverage.length / students.length) * 100;
};

server.post('/student/scores', validateInput, (req: Request, res: Response) => {
	const students: Student[] = req.body;

	const averageScore = calculateAverageScore(students);
	const highestScoringStudent = findHighestScoringStudent(students);
	const percentageAboveAverage = calculatePercentageAboveAverage(
		students,
		averageScore
	);

	const result = {
		averageScore,
		highestScoringStudent,
		percentageAboveAverage,
	};

	return res.json(result);
});

server.listen(3000);