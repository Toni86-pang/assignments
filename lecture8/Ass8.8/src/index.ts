import express, { Request, Response} from 'express';
import { middleware, unknownError } from './middleware';

const server = express();
server.use(express.json());

const students = [

];

server.use(middleware);

interface Student {
  id: string
  name: string
  email: string
}

server.post('/student', (req: Request, res: Response) => {
  if(!req.body.id || !req.body.name || !req.body.email) {
    console.log("error 400")
    res.status(400).send('400 error lol')
  } else {
    const student: Student = {
      id: req.body.id,
      name: req.body.name,
      email: req.body.email
  }
  students.push(student)
  res.status(201).send()
  }
});

server.get('/student/:id', (req: Request, res: Response) => {
	const id = req.params.id
  const student = students.find( student => student.id === id)
  
  if(student) {
    res.send(student)
  } else {
    res.status(404).send('Cant find dat ass')
  }
});

server.get('/student', (req: Request, res: Response) => {
  const studenIds = students.map(ele => ele.id)
	res.send(studenIds);
});


server.use(unknownError);

server.listen(3000)
  console.log('server optimal')

