import express, { Request, Response } from 'express';

const server = express();
server.use(express.json());


server.get('/', (_req: Request, res: Response) => {
    res.status(200).send('hello from docker');
});
  
const port = 3000;
server.listen(port, () => {
    console.log('Server listening on port', port);
});