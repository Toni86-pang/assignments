//10-10
import express, { Request, Response , NextFunction } from 'express';
import { errorHandler } from './middleware';

const server = express();
server.use(express.json());

// Define a username and password for authentication
const username = 'toni';
const password = 'salasana'; // Tehtävänanto: "Remember that secret information needs to be provided via environment variables."

const notes = [];

// Middleware to authenticate requests
const authenticate = (req: Request, res: Response, next: NextFunction ) => {
	const authHeader = req.headers['authorization'];
	if (authHeader) {
		const encodedCredentials = authHeader.split(' ')[1];
		const decodedCredentials = Buffer.from(encodedCredentials, 'base64').toString('utf-8');
		const [authUsername, authPassword] = decodedCredentials.split(':');
		if (authUsername === username && authPassword === password) { 
			return next();
		}
	}
	res.status(401).json({ error: 'Unauthorized' });
};
// Tää kannattaa laittaa middeware-tiedostoon kun sulla sellanen kerran on tuolla nätisti luotuna.

server.post('/notes', authenticate, (req, res) => {
	const { id, title, content } = req.body;
	const note = { id, title, content };
	notes.push(note);
	res.status(201).json(note);
});

server.get('/notes', authenticate, (req, res) => {
	res.json(notes);
});

server.delete('/notes/:id', authenticate, (req, res) => {
	const id = req.params.id;
	const noteIndex = notes.findIndex((note) => note.id.toString() === id);
	if (noteIndex !== -1) {
		const deletedNote = notes.splice(noteIndex, 1);
		res.json(deletedNote[0]);
	} else {
		res.status(404).json({ error: 'Note not found' });
	}
});

server.use(errorHandler);

server.listen(3000, () => {
	console.log('Server running on port 3000');
});

// Tehtävänanto: "Add secret note endpoints to your notice board." 
// Tässä on korvattu, eikä lisätty.