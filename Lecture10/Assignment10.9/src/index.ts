import express, { Request, Response } from 'express';

// Create an Express app
const app = express();

// Enable JSON request parsing
app.use(express.json());


const notes = []; // Tyyppi puuttuu


app.post('/notes', (req: Request, res: Response) => {
	const { id, title, contet } = req.body;
	const  note  = {id , title, contet}; // Tyyppi
	notes.push(note); // mielummin immutaabeli concat
	res.status(201).json(note);
	console.log(note);
});


app.get('/notes', (req: Request, res: Response) => {
	res.json(notes);
});

app.delete('/notes/:id', (req, res) => { // Tyypit puuttuu
	const id = req.params.id;
	const noteIndex = notes.findIndex((note) => note.id.toString() === id); // .find()
	if (noteIndex !== -1) {
		const deletedNote = notes.splice(noteIndex, 1); // mielummin immutaabeli filter
		console.log('DEBUG: its gone jimmy');
		res.json(deletedNote[0]);
	
	} else {
		res.status(404).json({ error: 'Note not found' });
	}
})
;

// Start the server
const port = 3000;
app.listen(port, () => {
	console.log(`Server is running on port ${port}`);
});