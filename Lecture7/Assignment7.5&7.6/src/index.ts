export default function calculator(operator: string, num1: number, num2: number): number | string {
    switch(operator) {
    case "+":
        return num1 + num2;
    case "-":
        return num1 - num2;
    case "*":
        return num1 * num2;
    case "/":
        if (num2 === 0) {
            return "Cannot divide by zero";
        } else {
            return num1 / num2;
        }
    default:
        return "Can't do that!";
    }
}

console.log(calculator("*", 2, 3));