import  calculator  from "../src/index";

describe("Division", () => {
    it("Returns 3 with parameters 25 and 5", () => {
        const result = calculator("/", 25, 5);
        expect(result).toBe(5);
    });
   
    it("Returns NaN with parameters 5 and NaN", () => {
        const result = calculator("/", NaN, 5);
        expect(result).toBe(NaN);
    });

    it("Returns cannot divide with 0 with parameters 5 and 0", () => {
        const result = calculator("/", 5, 0);
        expect(result).toBe("Cannot divide by zero");
    });
});