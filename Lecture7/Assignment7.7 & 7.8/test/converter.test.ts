import { convert } from "../src/index";  


describe("testing all the converions", () => {
    it("Conversion from litres to desiliters", () => {
        const result = convert( "4.2","l","dl");
        expect(result).toBe(42);
    });
    it("conversion from ounces to cups", () => {
        const result = convert("6.0","oz","cup");
        expect(result).toBe(1);
    });
    it("conversion from ounces to desilitres", () => {
        const result = convert("8.0","oz","dl");
        expect(result).toBe(2);
    });
    it("conversion from litres to ounces", () => {
        const result = convert("2.7","l","oz");
        expect(result).toBe(91);
    });
    it("conversion from cups to desilitres", () => {
        const result = convert("3.5","cup","dl");
        expect(result).toBe(8);
    });
    it("conversion from cups to pints", () => {
        const result = convert("3.2","cup","pint");
        expect(result).toBe(2);
    });
});