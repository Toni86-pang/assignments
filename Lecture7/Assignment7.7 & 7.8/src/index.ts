const amount = process.argv[2];
const sourceUnit = process.argv[3];
const targetUnit = process.argv[4];

const conversionTable: { [key: string]: number } = {
    dl: 0.1,
    l: 1,
    oz: 0.0295735,
    cup: 0.236588,
    pint: 0.473176
};


export function convert(amount: string, sourceUnit: string, targetUnit: string): number | null {
    const sourceRatio = conversionTable[sourceUnit];
    const targetRatio = conversionTable[targetUnit];

    if (sourceRatio !== undefined && targetRatio !== undefined) {
        const result = (Number(amount) * sourceRatio) / targetRatio;
        return Math.round(result);
    }

    return null;
}

if (amount && sourceUnit && targetUnit) {
    const result = convert(amount, sourceUnit, targetUnit);

    if (result !== null) {
        console.log(`Conversion result: ${result} ${targetUnit}` );
    } else {
        console.log("Invalid unit(s) specified.");
    }
} else {
    console.log("Invalid command. use  <amount> <source unit> <target unit>");
}