import  calculator  from "../src/index";

test("multiply 5 * 5 to be 25", () => {
    expect(calculator("*", 5, 5)).toBe(25);
});

test("multiply 0 * 5 to be 0", () => {
    expect(calculator("*", 0, 5)).toBe(0);
});

test("multiply -5 * 5 to be -25", () => {
    expect(calculator("*", -5, 5)).toBe(-25);
});

test("multiply with not a number is not possible", () => {
    expect(calculator("*", NaN, 5)).toBe(NaN);
});