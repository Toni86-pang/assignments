import  calculator  from "../src/index";

test("multiply 5 * 5 to be 25", () => {
    expect(calculator("*", 5, 5)).toBe(25);
});

test("multiply 0 * 5 to be 0", () => {
    expect(calculator("*", 0, 5)).toBe(0);
});

test("multiply -5 * 5 to be -25", () => {
    expect(calculator("*", -5, 5)).toBe(-25);
});

test("multiply with not a number is not possible", () => {
    expect(calculator("*", NaN, 5)).toBe(NaN);
});

test("sum of 5 + 5 to be 10", () => {
    expect(calculator("+", 5, 5)).toBe(10);
});

test("minus of 10 - 5 to be 15", () => {
    expect(calculator("-", 10, 5)).toBe(5);
});

test("division of 10 / 2 to be 5", () => {
    expect(calculator("/", 10, 2)).toBe(5);
});

test("division of 10 / 0 to be cannot divide by zero", () => {
    expect(calculator("/", 10, 0)).toBe("Cannot divide by zero");
});

test("everything else returns cant do that", () => {
    expect(calculator("", NaN, NaN)).toBe("Can't do that!");
});