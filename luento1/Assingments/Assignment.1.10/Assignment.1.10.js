//1.10
let lastName = "Rytivaara"
let age = 33
let isDoctor = true
let sender = "Essi esimerkki"

if (isDoctor) {
    console.log("Dear Dr ", lastName,)

    switch (age+1) {
        case 31:
            console.log("Congratulations on your 31st! Many happy returns!");
            break;
        case 32:
            console.log("Congratulations on your 32nd! Many happy returns!");
            break;
        case 33:
            console.log("Congratulations on your 33rd! Many happy returns!");
            break;
        default:
            console.log(`Congratulations on your ${age +1}th! Many happy returns!`);
            break;
    }
}
else {
    console.log("Dear Mx", lastName,)
    switch (age+1) {
        case 31:
            console.log("Congratulations on your 31st Many happy returns!");
            break;
        case 32:
            console.log("Congratulations on your 32nd Many happy returns!");
            break;
        case 33:
            console.log("Congratulations on your 33rd Many happy returns!");
            break;
        default:
            console.log(`Congratulations on your ${age +1}th! Many happy returns!`);
            break;
    }
}
console.log(`Sincerely ${sender}`);

// täällä on aika paljon koodin toistoa. 
// Congratulations etc.. on kirjotettu moneen kertaan, ja lisäksi koko koodiblokki if/else toistaa käytännössä saman asian.
// Ehdotan, että teet tehtävän uudestaan ilman koodin toistoa.