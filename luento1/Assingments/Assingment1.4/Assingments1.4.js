// global variablet consolille
const str1 = "Meikä";
const str2 = "Poikanen";
const char1 = str2[0];
const char2 = str2[7]; //kovakoodattu indeksi, tää on virheherkkä tapa koodata. jos str2 muuttuu, sun pitää muistaa muuttaa myös toi indeksi. mielummin käytä dynaamista indeksiä, joka katotaan suoraan siitä stringistä.
const str_sum = `Username ${str1} will be combined to ${str2}. thus it is ${str1 + str2}`;

console.log( str_sum );
console.log((str1 + str2).length /2); 
// lasket kahdesti str1 + str2, joten se kannattaisi tallettaa muuttujaan
console.log(str2.length); 
console.log(str1.toUpperCase());
console.log(char1 , char2);
