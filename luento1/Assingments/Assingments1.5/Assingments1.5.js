// global variablet consolille

//A
const pcount = 4 ;

if(pcount === 4){
    console.log(`true`)
}
else {
    console.log(`false`)
};

//B
const hasicecream = true;
const isStressed = false;

if (hasicecream || isStressed ) {
 console.log( `Mark is not stressed and is happy`)
}
else {
    console.log(`Mark is stressed and not happy!` )
};

//C
const sun = true;
const rain = false;
const temp = 20;

if ( sun  && !rain && temp >= 20) {
 console.log(`its beachday!`)
}
else {
    console.log(`get inside!`)
};

//D
const suzy = true;
const dan = false;

if(!((suzy && dan) || (!suzy && !dan))) {
    // Mä luotan siihen, että matemaattinen logiikka pelaa, mutta aika monta not-operaattoria tässä on, vähän vaikea lukea
    console.log( `is happy arin!`)
}
else {
    console.log(` is unhappy arin!`)
};
