"use strict";
function findHighestScoringStudent(students) {
    let highestScore = students[0].score;
    let highestScoringStudent = students[0];
    for (let i = 1; i < students.length; i++) {
        if (students[i].score > highestScore) {
            highestScore = students[i].score;
            highestScoringStudent = students[i];
        }
    }
    return highestScoringStudent;
}
function findLowestScoringStudent(students) {
    let lowestScore = students[0].score;
    let lowestScoringStudent = students[0];
    for (let i = 1; i < students.length; i++) {
        if (students[i].score < lowestScore) {
            lowestScore = students[i].score;
            lowestScoringStudent = students[i];
        }
    }
    return lowestScoringStudent;
}
function calculateAverageScore(students) {
    let totalScore = 0;
    for (let i = 0; i < students.length; i++) {
        totalScore += students[i].score;
    }
    return totalScore / students.length;
}
function assignGrade(score) {
    if (score >= 95 && score <= 100) {
        return "5";
    }
    else if (score >= 80 && score <= 94) {
        return "4";
    }
    else if (score >= 60 && score <= 79) {
        return "3";
    }
    else if (score >= 40 && score <= 59) {
        return "2";
    }
    else {
        return "1";
    }
}
function printStudentsAboveAverage(students) {
    const averageScore = calculateAverageScore(students);
    console.log("Average Score:", averageScore.toFixed(2));
    console.log("Students Above Average:");
    for (let i = 0; i < students.length; i++) {
        if (students[i].score > averageScore) {
            console.log(`${students[i].name} - Score: ${students[i].score} - Grade: ${students[i].grade}`);
        }
    }
}
const students = [
    { name: 'Markku', score: 99, grade: "" },
    { name: 'Karoliina', score: 58, grade: "" },
    { name: 'Susanna', score: 69, grade: "" },
    { name: 'Benjamin', score: 77, grade: "" },
    { name: 'Isak', score: 49, grade: "" },
    { name: 'Liisa', score: 89, grade: "" },
];
const highestScoringStudent = findHighestScoringStudent(students);
const lowestScoringStudent = findLowestScoringStudent(students);
console.log("Highest Scoring Student:", highestScoringStudent.name);
console.log("Lowest Scoring Student:", lowestScoringStudent.name);
for (let i = 0; i < students.length; i++) {
    students[i].grade = assignGrade(students[i].score);
}
printStudentsAboveAverage(students);
