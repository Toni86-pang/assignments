"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const axios_1 = __importDefault(require("axios"));
function fetchData() {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            const [todosResponse, usersResponse] = yield Promise.all([
                axios_1.default.get("https://jsonplaceholder.typicode.com/todos/"),
                axios_1.default.get("https://jsonplaceholder.typicode.com/users/"),
            ]);
            const todos = todosResponse.data;
            const users = usersResponse.data.reduce((acc, user) => {
                acc[user.id] = user;
                return acc;
            }, {});
            const modifiedTodos = todos.map((todo) => {
                const { userId } = todo, rest = __rest(todo, ["userId"]);
                const user = users[userId];
                const modifiedUser = {
                    name: user.name,
                    username: user.username,
                    email: user.email,
                };
                return Object.assign(Object.assign({}, rest), { user: modifiedUser });
            });
            console.log(modifiedTodos);
        }
        catch (error) {
            console.error("Error fetching data:", error);
        }
    });
}
fetchData();
