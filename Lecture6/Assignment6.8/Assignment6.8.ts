import axios from "axios"

interface Todo {
  id: number;
  userId: number;
  title: string;
  completed: boolean;
}

interface User {
  id: number;
  name: string;
  username: string;
  email: string;
}

async function fetchTodos(): Promise<Todo[]> {
    const response = await axios.get("https://jsonplaceholder.typicode.com/todos/")
    return response.data
}

async function fetchUsers(): Promise<User[]> {
    const response = await axios.get("https://jsonplaceholder.typicode.com/users/")
    return response.data
}

async function fetchData() {
    try {
        const [todos, users] = await Promise.all([fetchTodos(), fetchUsers()])
        const usersMap: { [userId: number]: User } = {}
        for (const user of users) {
            usersMap[user.id] = user
        }

        const modifiedTodos = todos.map((todo) => {
            const { userId, ...rest } = todo
            const user = usersMap[userId]
            const { name, username, email } = user
            return { ...rest, user: { name, username, email } }
        })

        console.log(modifiedTodos)
    } catch (error) {
        console.error("Error:", error.message)
    }
}

fetchData()
//tämän version pitäisi nyt olla toimivampi ja määrittelyt pitäsi olla paremmin. tässä oppi nyt taas hiukan lisää määrittelystä ja kartoittamisesta.

// Ehdotan, että teet tämän uudestaan kiinnittäen huomiota tyyppeihih, ja siihen, että ymmärrät missä vaiheessa tyypit määritellään.
// Muista, että VSC näyttää tyypi kun vie hiiren kursorin muuttujan päälle, se on aina helppo tarkistaa