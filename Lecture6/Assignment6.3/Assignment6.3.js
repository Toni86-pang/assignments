"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const getValue = function () {
    return new Promise((resolve) => {
        setTimeout(() => {
            resolve(Math.random());
        }, Math.random() * 1500);
    });
};
const ValuesWithAwait = () => __awaiter(void 0, void 0, void 0, function* () {
    const value1 = yield getValue();
    const value2 = yield getValue();
    console.log(value1, value2);
});
const ValuesWithThen = () => {
    let value1;
    getValue()
        .then(result => {
        value1 = result;
        return getValue();
    })
        .then(result => {
        console.log(value1, result);
    });
};
ValuesWithAwait();
ValuesWithThen();
