const getValue = function (): Promise<number> {
  return new Promise((resolve) => {
      setTimeout(() => {
          resolve(Math.random())
      }, Math.random() * 1500)
  })
}

const ValuesWithAwait = async () => {
  const value1 = await getValue()
  const value2 = await getValue()
  console.log(value1 ,value2)
}

const ValuesWithThen = () => {
  let value1: number
  getValue()
      .then(result => {
          value1 = result
          return getValue()
      })
      .then(result => {
          console.log(value1, result)

      })

}

ValuesWithAwait()
ValuesWithThen()
