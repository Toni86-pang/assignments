function delay(ms: number): Promise<void> {
    return new Promise((resolve) => setTimeout(resolve, ms));
}

async function raceLap(): Promise<number> {
    return new Promise((resolve, reject) => {
        const hasCrashed = Math.random() < 0.03;
        if (hasCrashed) {
            reject("Crashed");
        } else {
            const lapTime = Math.floor(Math.random() * 6 + 20); // Random lap time between 20-25 seconds // floor tarvitaan vain, jos halutaan tasasekunteja
            resolve(lapTime);
            resolve(lapTime);
        }
    });
}

async function race(drivers: string[], laps: number): Promise<string> { // tämä funktio itse asiassa palauttaa vaan stringin, joten Promise tässä kohtaa on turha
    const results: { [driver: string]: { totalTime: number; bestLapTime: number } } = {}; // tee tyypille oma interface
    let lapTimes: { [driver: string]: number };

    for (let lap = 1; lap <= laps; lap++) {
        lapTimes = {};

        await Promise.all(
            drivers.map(async (driver) => {
                try {
                    const lapTime = await raceLap();
                    lapTimes[driver] = lapTime;

                    if (!results[driver]) {
                        results[driver] = {
                            totalTime: lapTime,
                            bestLapTime: lapTime,
                        };
                    } else {
                        results[driver].totalTime += lapTime;
                        if (lapTime < results[driver].bestLapTime) {
                            results[driver].bestLapTime = lapTime;
                        }
                    }
                } catch (error) {
                    console.log(`${driver} crashed on lap ${lap}`);
                }
                // ^ eiks tässä ajaja pääse mukaan seuraavalle kierrokselle, vaikka se olis edellisellä crashannu?
            })
        );

        console.log(`Lap ${lap} times:`, lapTimes);
    }

    const winner = Object.entries(results).reduce((prev, [driver, stats]) =>
        stats.totalTime < prev.stats.totalTime ? { driver, stats } : prev,
        { driver: "", stats: { totalTime: Infinity, bestLapTime: Infinity } }
    );
    // ^ Tätä piti jo tovi tavata että ymmärsi mitä tapahtuu. Eikö olisi ollut selkeämpää, jos results-objektin sisällä olisi ollut myös ajajan nimi? 

    return `Winner: ${winner.driver} - Total Time: ${winner.stats.totalTime}s - Best Lap: ${winner.stats.bestLapTime}s`;
}

// Example usage:
const drivers = ["Driver 1", "Driver 2", "Driver 3"];
const laps = 5;

race(drivers, laps)
    .then((winner) => console.log(winner))
    .catch((error) => console.log("Race aborted:", error));

// Asiat vaikuttaa olevan hallussa, mutta selkeyttä kaipaisin edelleen lisää. Erityisesti interfacet tai classit käyttöön selkeyttämään sitä, minkälaista dataa milloinkin ollaan käsittelemässä.
