"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
function delay(ms) {
    return new Promise((resolve) => setTimeout(resolve, ms));
}
function raceLap() {
    return __awaiter(this, void 0, void 0, function* () {
        return new Promise((resolve, reject) => {
            const hasCrashed = Math.random() < 0.03;
            if (hasCrashed) {
                reject("Crashed");
            }
            else {
                const lapTime = Math.floor(Math.random() * 6 + 20); // Random lap time between 20-25 seconds
                resolve(lapTime);
            }
        });
    });
}
function race(drivers, laps) {
    return __awaiter(this, void 0, void 0, function* () {
        const results = {};
        let lapTimes;
        for (let lap = 1; lap <= laps; lap++) {
            lapTimes = {};
            yield Promise.all(drivers.map((driver) => __awaiter(this, void 0, void 0, function* () {
                try {
                    const lapTime = yield raceLap();
                    lapTimes[driver] = lapTime;
                    if (!results[driver]) {
                        results[driver] = {
                            totalTime: lapTime,
                            bestLapTime: lapTime,
                        };
                    }
                    else {
                        results[driver].totalTime += lapTime;
                        if (lapTime < results[driver].bestLapTime) {
                            results[driver].bestLapTime = lapTime;
                        }
                    }
                }
                catch (error) {
                    console.log(`${driver} crashed on lap ${lap}`);
                }
            })));
            console.log(`Lap ${lap} times:`, lapTimes);
        }
        const winner = Object.entries(results).reduce((prev, [driver, stats]) => stats.totalTime < prev.stats.totalTime ? { driver, stats } : prev, { driver: "", stats: { totalTime: Infinity, bestLapTime: Infinity } });
        return `Winner: ${winner.driver} - Total Time: ${winner.stats.totalTime}s - Best Lap: ${winner.stats.bestLapTime}s`;
    });
}
// Example usage:
const drivers = ["Driver 1", "Driver 2", "Driver 3"];
const laps = 5;
race(drivers, laps)
    .then((winner) => console.log(winner))
    .catch((error) => console.log("Race aborted:", error));
