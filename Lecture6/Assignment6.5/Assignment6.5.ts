import axios from "axios";

interface Movie {
  Title: string;
  Year: string;
}

async function searchMovie(title: string, year?: number): Promise<void> {
  try {
    const response = await axios.get("http://www.omdbapi.com/", {
      params: {
        apikey: "c3a0092f", // Replace with your actual API key
        s: title,
        y: year,
      },
    });

    const data = response.data;
    if (data.Response === "True") {
      const movies: Movie[] = data.Search;
      console.log("Search Results:");
      movies.forEach((movie) => {
        console.log("Title:", movie.Title);
        console.log("Year:", movie.Year);
     
      });
    } else {
      console.log("No movies found.");
    }
  } catch (error) {
    console.error("Error searching movie:", error);
  }
}

searchMovie("Inception", 2010);