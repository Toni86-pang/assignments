
function findLargestnumber (array: number[]): number {
    let largest = array[0];
    for (let i = 0; i < array.length; i++ ) {
        if (array[i] > largest) {
            largest = array[i];
        }
    }
    return largest;
}

function findSecondLargestNumber (array: number[]): number {
    let largest = array[0];
    let secondLargest = array[1];

    if (secondLargest > largest) {
        [largest, secondLargest] = [secondLargest, largest];
    }

    for (let i = 2; i < array.length; i++) {
        if (array[i] > largest) {
            secondLargest = largest;
            largest = array[i];
        } else if (array[i] > secondLargest && array[i] < largest) {
            secondLargest = array[i];
        }
    }

    return secondLargest;
}

const numbers = [1, 5, 8, 26, -3, 55];
const largest = findLargestnumber(numbers);
const secondLargest = findSecondLargestNumber(numbers);

console.log("Largest number:", largest);
console.log("Second largest number", secondLargest)