import { useState, ChangeEvent ,useEffect } from 'react'


    const [time, setTime] = useState(null);  
    useEffect(() => {
        // this gets called on component
        // mount, and also when the prop
        // counter changes
        setTime(new Date());
    }, [props.counter]);
  
    return <div>{time}</div>
  
  
  export const [count, setCount] = useState(0);
  
  useEffect(() => {
    timeout = setTimeout(() => setCount(count + 1));
  
    // return a function that React can call to clear our timeout
    //   the next time this effect is updated
    return () => clearTimeout(timeout);
  }, [count]);


