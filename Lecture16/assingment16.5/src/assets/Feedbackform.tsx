import { useState, FormEvent, SetStateAction } from 'react'


const Feedback = () => {
    const [name, setName] = useState("");
    const [email, setEmail] = useState("");
    const [feedback, setFeedback] = useState("");
    const [choiseOption, setChoiceOption] = useState("");
  
    const handleRadioChange = (event: { target: { value: SetStateAction<string>; }; }) => {
        setChoiceOption(event.target.value)
    }

    const onSubmit = (e:FormEvent<HTMLFormElement>) => {
        e.preventDefault(); 
        console.log(name, email, feedback)
    };
    const resetButton = () => {
        setName("")
        setEmail("")
        setFeedback("")
        setChoiceOption("") 
    }
    return (
        <form onSubmit={onSubmit}>
            
            <input type="radio" 
            value="feedback"
            checked={choiseOption === "feedback"}
            onChange={handleRadioChange} />
            <br></br>
            <input type="radio" 
            value="suggestion"
            checked={choiseOption === "suggestion"}
            onChange={handleRadioChange}/>
            <br></br>
            <input type="radio" 
            value="question"
            checked={choiseOption === "question"}
            onChange={handleRadioChange} />
            <br></br>
            
            <br></br>
            <input type="input" placeholder="name" value={name} onChange={e => { setName(e.target.value); }} />
            <input type="input" placeholder="email"value={email} onChange={e => { setEmail(e.target.value); }} />
            <br></br><br></br>
            <textarea value={feedback} placeholder="Type your feedback" onChange={e => { setFeedback(e.target.value); }} />
            <br></br>
            <input type="submit" value="Send" />
            <button onClick={resetButton}>Reset</button>
        </form>
    )
};


export default Feedback