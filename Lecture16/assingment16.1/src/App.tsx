import { useState, ChangeEvent } from "react";

interface Props {
  addNumber: (number: string) => void;
}

function Input({ addNumber }: Props) {
  const [number, setNumber] = useState("");

  const onChange = (event: ChangeEvent<HTMLInputElement>) => {
    const inputNumber = event.target.value;
    
    setNumber(inputNumber)

    if (inputNumber.length === 10) {
      addNumber(inputNumber); 
      setNumber(""); 
    }
  };

  return (
    <div className="Input">
      <input type="number" value={number} onChange={onChange} maxLength={10} />
    </div>
  );
}

function App() {
  const [numbers, setNumbers] = useState<string[]>([]);

  const addNumber = (number: string) => {
    setNumbers((prevNumbers) => [...prevNumbers, number]);
  };

  return (
    <>
      <Input addNumber={addNumber} />
      {numbers.map((number, index) => (
        <div key={index}>{number}</div>
      ))}
    </>
  );
}

export default App;
