import { useState, ChangeEvent } from 'react'


function Phonenumber(props:string) {

const = 

const [phonenumb, setPhonenumb] = useState('')


const onInputChange = (event: ChangeEvent<HTMLInputElement>) => {
    phonenumb.length >= 10 
        setPhonenumb(event.target.value)
    }

  return (
    <div className='Phone_input'>
      <input type='number'
        value={phonenumb} 
        onChange={onInputChange}/>
      </div>
  )
}

export default Phonenumber