import { useState } from 'react';
import './App.css';

function App() {
  const [newball, setNewball] = useState<Array<number>>([]);

  const handleClicker = () => {
    const number = Math.floor(Math.random() * (100 - 0));
    setNewball((newball) => [...newball, number]);
  };

  const newBingoBall = newball.map((bingoball) => (
    <div className='bingoball' key={bingoball}>
      {bingoball}
    </div>
  ));

  return (
    <>
      <div>
        <h1>Lets go bingo!</h1>
        <div className='bingo'>{newBingoBall}</div>
        <button onClick={handleClicker}>+</button>
      </div>
    </>
  );
}

export default App;
