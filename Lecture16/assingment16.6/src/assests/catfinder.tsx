import { useState, useEffect, SetStateAction } from "react";

const CatFinder = () => {
  const [catPicUrl, setCatPicUrl] = useState("");
  const [textInput, setTextInput] = useState("");

  useEffect(() => {
    loadRandomCatPic();
  }, []);

  const loadRandomCatPic = () => {
    let url = "https://cataas.com/cat";
    if (textInput !== "") {
      url += `/says/${textInput}`;
    }
    setCatPicUrl(url);
  };

  const handleReloadClick = () => {
    if (textInput === "") {
      loadRandomCatPic();
    } else {
      setTextInput("");
      loadRandomCatPic();
    }
  };

  const handleInputChange = (event: { target: { value: SetStateAction<string>; }; }) => {
    setTextInput(event.target.value);
  };

  return (
    <div>
      <img src={catPicUrl} alt="Random Cat" />
      <input
        type="text"
        value={textInput}
        onChange={handleInputChange}
        placeholder="Enter text..."
      />
      <button onClick={handleReloadClick}>Reload</button>
    </div>
  );
};

export default CatFinder
