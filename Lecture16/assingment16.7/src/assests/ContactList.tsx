import React, { useState } from "react";
import "./ContactList.css";
import ContactForm from "./ContactForm";
import ContactDetails from "./ContactDetails";

export interface Contact {
  id: number;
  name: string;
  email: string;
  phone: string;
  address: string;
  website: string;
  info: string;
}

const ContactList = () => {
  const [contacts, setContacts] = useState<Contact[]>([]);
  const [selectedContact, setSelectedContact] = useState<Contact | null>(null);
  const [isAddingContact, setIsAddingContact] = useState(false);
  const [searchTerm, setSearchTerm] = useState("");

  const handleAddContact = () => {
    setIsAddingContact(true);
    setSelectedContact(null);
  };

  const handleSaveContact = (newContact: Contact) => {
    if (selectedContact) {
      // Update existing contact
      const updatedContacts = contacts.map((contact) =>
        contact.id === selectedContact.id ? newContact : contact
      );
      setContacts(updatedContacts);
    } else {
      // Add new contact
      const newContacts = [...contacts, newContact];
      setContacts(newContacts);
    }

    setIsAddingContact(false);
    setSelectedContact(null);
  };

  const handleCancel = () => {
    setIsAddingContact(false);
    setSelectedContact(null);
  };

  const handleContactClick = (contact: Contact) => {
    setSelectedContact(contact);
    setIsAddingContact(false);
  };

  const handleEditContact = () => {
    setIsAddingContact(true);
  };

  const handleRemoveContact = () => {
    if (selectedContact) {
      const updatedContacts = contacts.filter(
        (contact) => contact.id !== selectedContact.id
      );
      setContacts(updatedContacts);
      setSelectedContact(null);
    }
  };

  const handleSearchChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setSearchTerm(event.target.value);
  };

  const filteredContacts = contacts.filter((contact) =>
    contact.name.toLowerCase().includes(searchTerm.toLowerCase())
  );

  return (
    <div className="container">
      <div className="column">
        <h2 className="label">Contacts</h2>
        <input className="find"
          type="text"
          value={searchTerm}
          onChange={handleSearchChange}
          placeholder="Search contacts..."
        />
        <ul>
          {filteredContacts.map((contact) => (
            <li key={contact.id} onClick={() => handleContactClick(contact)}>
              {contact.name}
            </li>
          ))}
        </ul>
        <button onClick={handleAddContact}>Add contact</button>
      </div>
      <div className="column">
        {isAddingContact || selectedContact ? (
          <ContactForm
            contact={selectedContact}
            onSave={handleSaveContact}
            onCancel={handleCancel}
          />
        ) : (
          <div>
            {selectedContact ? (
              <ContactDetails
                contact={selectedContact}
                onEdit={handleEditContact}
                onRemove={handleRemoveContact}
              />
            ) : (
              <p>Select a contact to view details</p>
            )}
          </div>
        )}
      </div>
    </div>
  );
};

export default ContactList;