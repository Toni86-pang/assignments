import React, { useState } from "react";
import { Contact } from "./ContactList";

interface ContactFormProps {
  contact: Contact | null;
  onSave: (newContact: Contact) => void;
  onCancel: () => void;
}

const ContactForm = ({ contact, onSave, onCancel }: ContactFormProps) => {
  const [name, setName] = useState(contact ? contact.name : "");
  const [email, setEmail] = useState(contact ? contact.email : "");
  const [phone, setPhone] = useState(contact ? contact.phone : "");
  const [address, setAddress] = useState(contact ? contact.address : "");
  const [website, setWebsite] = useState(contact ? contact.website : "");
  const [info, setInfo] = useState(contact ? contact.info : "");

  const handleSubmit = (event: React.FormEvent) => {
    event.preventDefault();

    const newContact: Contact = {
      id: contact ? contact.id : Date.now(),
      name,
      email,
      phone,
      address,
      website,
      info,
    };

    onSave(newContact);
  };

  return (
    <div>
      <h2 className="label">{contact ? "Edit Contact" : "Add Contact"}</h2>
      <form onSubmit={handleSubmit}>
        <label className="label">Name:</label>
          <input
            type="text"
            value={name}
            onChange={(e) => setName(e.target.value)}
          />
        
        <label className="label">Email:</label>
          <input
            type="text"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
          />
        
        <label className="label"> Phone:</label>
          <input
            type="text"
            value={phone}
            onChange={(e) => setPhone(e.target.value)}
          />
        
        <label className="label">Address:</label>
          <input
            type="text"
            value={address}
            onChange={(e) => setAddress(e.target.value)}
          />
        
        <label className="label"> Website: </label>
          <input
            type="text"
            value={website}
            onChange={(e) => setWebsite(e.target.value)}
          />
        
        <label className="label">Info: </label>
          <textarea value={info} onChange={(e) => setInfo(e.target.value)} />

        <br></br>
        <button type="submit">Save</button>
        <button type="button" onClick={onCancel}>
          Cancel
        </button>
      </form>
    </div>
  );
};

export default ContactForm;