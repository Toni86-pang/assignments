import { Contact } from "./ContactList";

interface ContactDetailsProps {
  contact: Contact;
  onEdit: () => void;
  onRemove: () => void;
}

const ContactDetails = ({
  contact,
  onEdit,
  onRemove,
}: ContactDetailsProps) => {
  const { name, email, phone, address, website, info } = contact;

  return (
    <div className="details">
      <h2>Contact Details</h2>
      <p>Name: {name}</p>
      <p>Email: {email}</p>
      <p>Phone: {phone}</p>
      <p>Address: {address}</p>
      <p>Website: {website}</p>
      <p>Info: {info}</p>
      <button className="remove" onClick={onEdit}>
        Edit
      </button>
      <button className="remove" onClick={onRemove}>
        Remove
      </button>
    </div>
  );
};

export default ContactDetails;
