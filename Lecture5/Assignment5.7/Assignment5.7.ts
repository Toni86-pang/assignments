import fs from "fs";

function replaceWords(text: string): string {
    const replacedText = text
        .replace(/joulu/g, "kinkku")
        .replace(/lapsilla/g, "poroilla");

    return replacedText;
}

function processFile(filePath: string): void {
    const fileContent = fs.readFileSync(filePath, "utf-8");
    const modifiedContent = replaceWords(fileContent);
  
    console.log(modifiedContent);
  
    fs.writeFileSync("./uusi_joulu.txt", modifiedContent);
}

const filePath = "./joulu.txt";
processFile(filePath);
