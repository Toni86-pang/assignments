"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const fs_1 = __importDefault(require("fs"));
function replaceWords(text) {
    const replacedText = text
        .replace(/joulu/g, "kinkku")
        .replace(/lapsilla/g, "poroilla");
    return replacedText;
}
function processFile(filePath) {
    const fileContent = fs_1.default.readFileSync(filePath, "utf-8");
    const modifiedContent = replaceWords(fileContent);
    console.log(modifiedContent);
    fs_1.default.writeFileSync("./uusi_joulu.txt", modifiedContent);
}
const filePath = "./joulu.txt";
processFile(filePath);
