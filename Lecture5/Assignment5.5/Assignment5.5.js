"use strict";
class Ingredient {
    constructor(name, amount) {
        this.name = name;
        this.amount = amount;
    }
    scale(factor) {
        this.amount *= factor;
    }
}
class Recipe {
    constructor(name, servings, ingredients) {
        this.name = name;
        this.servings = servings;
        this.ingredients = ingredients;
    }
    setServings(newServings) {
        const scaleFactor = newServings / this.servings;
        this.servings = newServings;
        this.ingredients.forEach(ingredient => {
            ingredient.scale(scaleFactor);
        });
    }
    toString() {
        const ingredientList = this.ingredients.map(ingredient => `${ingredient.name} - ${ingredient.amount}`);
        return `
        Recipe: ${this.name}
        Servings: ${this.servings}
        Ingredients:
        ${ingredientList.join("\n")}
      `;
    }
}
class HotRecipe extends Recipe {
    constructor(name, servings, ingredients, heatLevel) {
        super(name, servings, ingredients);
        this.heatLevel = heatLevel;
    }
    toString() {
        let warning = "";
        if (this.heatLevel > 5) {
            warning = "WARNING: This recipe is very hot!";
        }
        const ingredientList = this.ingredients.map(ingredient => `${ingredient.name} - ${ingredient.amount}`);
        return `
        Recipe: ${this.name}
        Servings: ${this.servings}
        Heat Level: ${this.heatLevel}
        ${warning}
        Ingredients:
        ${ingredientList.join("\n")}
      `;
    }
}
const maito = new IngredientObject("maito", 7);
const pottu = new IngredientObject("pottu", 2);
const suola = new IngredientObject("suola", 1);
const voi = new IngredientObject("voi", 2);
const potato = new recipe_maker("muussia", 6, [maito, pottu, suola, voi]);
console.log(potato.toString());
const spicypotato = new HotRecipe("Äkästä muussia", 4, [maito, pottu, suola, voi], 3);
spicypotato.setServings(100);
console.log(spicypotato.toString());
const superHotpotato = new HotRecipe("Liian äkästä muussia", 8, [maito, pottu, suola, voi], 8);
superHotpotato.setServings(100);
console.log(superHotpotato.toString());
