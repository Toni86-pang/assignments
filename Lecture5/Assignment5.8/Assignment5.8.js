"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const fs_1 = __importDefault(require("fs"));
function modifyForecastTemperature(day, newTemp) {
    const fileContent = fs_1.default.readFileSync("./forecast_data.json", "utf-8");
    const data = JSON.parse(fileContent);
    data.temperature = newTemp;
    fs_1.default.writeFileSync("./forecast_data.json", JSON.stringify(data));
}
modifyForecastTemperature("monday", 30);
