import fs from "fs";
  
function modifyForecastTemperature(day: string, newTemp: number): void { // day muuttujaa ei käytetä, joten se kannattaa poistaa
    const fileContent = fs.readFileSync("./forecast_data.json", "utf-8");
    const data = JSON.parse(fileContent);

    data.temperature = newTemp;
  
    fs.writeFileSync("./forecast_data.json", JSON.stringify(data));
}

modifyForecastTemperature("monday", 30);