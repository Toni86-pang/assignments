class IngredientObject { // Nimeämisestä, koska tämä on objekti, sen nimeäminen eksplisiittisesti objektiksi on vähän turhaa. 
    name: string;
    ammount : number;
    
    constructor( name : string, ammount :number ) { 
        this.name = name;
        this.ammount = ammount;
    }

    scale = function ( factor: number){ // väärä syntaksi --> scale(factor: number), tarkista slidet
        this.ammount = factor * this.ammount;
    };
}

class Recipes { // Nimeämisestä, yleinen konventio on, että koska tähän objektiin tallentuu vain yksi resepti, sen nimi olisi yksikössä
    rname : string; // tämä on resepti-luokan nimi, joten sen nimi voi huoletta olla vain "name", mikä on helpompi muistaa eikä niin virhealtis
    servings: number;
    ingredient: Array<{name:string, ammount: number}>;
        
    constructor( name: string, servings: number ,ingredient: Array<{name:string, ammount: number }>) { 
        this.rname = name;
        this.servings = servings;
        this.ingredient = ingredient;
    }
    toString() { // täällä oikea syntaksi
        return this.ingredient.reduce((acc:string, cur: {name:string, ammount:number}) => {
            return acc + `- ${cur.name} (${cur.ammount})\n`;
        }, `${this.rname} (${this.servings} servings)\n\n`);
    }
}

const maito = new IngredientObject("maito", 7);
const pottu = new IngredientObject("pottu", 2);
const suola = new IngredientObject("suola", 1);
const voi = new IngredientObject("voi", 2);

const muussi = new recipe_maker("muussia", 6, [maito, pottu, suola, voi]);
    
console.log(muussi.toString());