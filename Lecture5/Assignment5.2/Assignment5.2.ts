//object constructor for ingredients
function IngredientsObjects(this: any, name: string, amount:number) {
    this.name = name;
    this.amount = amount;
}


//object constructor for recipe
function RecipeObjects(this: any, name: string, ingredients: Array<{name:string, amount:number}>, servings: number, ) {
    this.name = name;
    this.ingredients = ingredients;
    this.servings = servings;
    this.toString = function () {
        return this.ingredients.reduce((acc: string, cur: { name: string, amount: number }) => {
            return acc + `- ${cur.name} (${cur.amount})\n`
        }, `${this.name} (${this.servings} servings)\n\n`)
    }
}
   //lisätty to string  koska epähuomiossa oli väärä versio.
    // toString puuttuu


const pottu = new IngredientObject("pottu", 2); // täällä on VSC punakynää!
const suola = new IngredientObject("suola", 1);

const muussi = new recipe_maker("muussia", 6, [ pottu, suola]);

console.log(muussi);