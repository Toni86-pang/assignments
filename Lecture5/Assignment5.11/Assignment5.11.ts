const numbers: number[] = [1,5,9,3];

function aboveAverage (numbers: number[]){
    const sum = numbers.reduce((acc, num)=> acc + num , 0);
    const average = sum / numbers.length;
    
    const aboveAverageValue = numbers.filter(num => num > average);
    return aboveAverageValue;
}

const result: number[] = aboveAverage(numbers); // eksplisiittistä tyyppiä ei tarvita, koska se tulee funktion paluuarvosta
console.log(result);