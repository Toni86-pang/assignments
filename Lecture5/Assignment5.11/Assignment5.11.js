"use strict";
const numbers = [1, 5, 9, 3];
function aboveAverage(numbers) {
    const sum = numbers.reduce((acc, num) => acc + num, 0);
    const average = sum / numbers.length;
    const aboveAverageValue = numbers.filter(num => num > average);
    return aboveAverageValue;
}
const result = aboveAverage(numbers);
console.log(result);
