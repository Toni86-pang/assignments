const str = process.argv[2];

function getVowelcount(str: string): number {
    const vowels = ["a", "e", "i", "o", "u" , "y"];
    let count = 0;

    for(let i=0; i < str.length; i++ ) {
        if(vowels.includes(str[i].toLowerCase())){
            count++;
        }
    }
    return count;
}

console.log(getVowelcount(str));

// *thumbsup*