"use strict";
const ekanimi = "Toni";
const sukunimi = "Vainionpää";
const [username, password] = generoija(ekanimi, sukunimi);
function generoija(ekanimi, sukunimi) {
    const currentYear = new Date().getFullYear().toString().slice(-2);
    const username = `B${currentYear}${ekanimi.slice(0, 2).toLowerCase()}${sukunimi.slice(0, 2).toLowerCase()}`;
    const randomLetter = String.fromCharCode(65 + Math.floor(Math.random() * 26));
    const randomChar = String.fromCharCode(33 + Math.floor(Math.random() * 15));
    const password = `${randomLetter}${ekanimi.charAt(0).toLowerCase()}${sukunimi.charAt(sukunimi.length - 1).toUpperCase()}${randomChar}${currentYear}`;
    return [username, password];
}
console.log(username, password);
