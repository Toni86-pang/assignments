"use strict";
const n = 4;
const steps = calculateSteps(n);
function calculateSteps(n) {
    let steps = 0;
    while (n !== 1) {
        if (n % 2 === 0) {
            n = n / 2;
        }
        else {
            n = n * 3 + 1;
        }
        steps++;
    }
    return steps;
}
console.log(steps);


// Suosittelen kattomaan noi luokkajutut vielä uudestaan ajatuksella läpi jos ja kun on aikaa. Kaikenkaikkiaan näyttääpi oikein hyvältä.