"use strict";
function IngredientObject(name, ammount) {
    this.name = name;
    this.ammount = ammount;
}
IngredientObject.prototype.scale = function (factor) {
    this.ammount = factor * this.ammount;
};
function recipe_maker(name, servings, ingredient) {
    this.rname = name;
    this.servings = servings;
    this.ingredient = ingredient;
}
recipe_maker.prototype.toString = function () {
    return this.ingredient.reduce((acc, cur) => {
        return acc + `- ${cur.name} (${cur.ammount})\n`;
    }, `${this.name} (${this.servings} servings)\n\n`);
};
const maito = new IngredientObject("maito", 7);
const pottu = new IngredientObject("pottu", 2);
const suola = new IngredientObject("suola", 1);
const voi = new IngredientObject("voi", 2);
const muussi = new recipe_maker("muussia", 6, [maito, pottu, suola, voi]);
console.log(muussi.toString());
