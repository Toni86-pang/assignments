"use strict";
function likes(names) {
    const length = names.length;
    if (length === 0) {
        return "no one likes this";
    }
    else if (length === 1) {
        return `${names[0]} likes this`;
    }
    else if (length === 2) {
        return `${names[0]} and ${names[1]} like this`;
    }
    else if (length === 3) {
        return `${names[0]}, ${names[1]} and ${names[2]} like this`;
    }
    else {
        const remainingCount = length - 2;
        return `${names[0]}, ${names[1]} and ${remainingCount} others like this`;
    }
}
console.log(likes([]));
console.log(likes(["John"]));
console.log(likes(["Mary", "Alex"]));
console.log(likes(["John", "James", "Linda"]));
console.log(likes(["Alex", "Linda", "Mark", "Max"]));
